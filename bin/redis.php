<?php
$worker = new GearmanWorker();
$worker->addServer();
$worker->addFunction('updateEquipmentToRedis', 'syncToRedis');
$worker->addFunction('insertEquipmentToRedis', 'syncToRedis');
$worker->addFunction('deleteEquipmentToRedis', 'syncToRedisDel');
$worker->addFunction('insertEquipmentStatusToRedis', 'syncToRedis2');
$worker->addFunction('updateEquipmentStatusToRedis', 'syncToRedis2');



$redis = new Redis();
$redis->connect('127.0.0.1', 6379);
$redis->auth('yh_redis');
$redis->select(8);

while($worker->work());
function ($job)
{
        global $redis;
        $workString = $job->workload();
        var_dump("redis---");
        var_dump($workString);
        $work = json_decode($workString);
        if(!isset($work->equipment_number)){
                return false;
        }
        $redis->zadd('deviceEqNumber'.$work->id,$work->install_time,$work->equipment_number);
        $redis->hset('deviceList',$work->equipment_number, $workString);
}

function syncToRedisDel($job)
{
    global $redis;
    $workString = $job->workload();
    $work = json_decode($workString);
    //var_dump($workString);
    if(!isset($work->equipment_number)){
        return false;
    }
    $redis->hDel('deviceList',$work->equipment_number);
}

function syncToRedis2($job)
{   
        global $redis;
        $workString = $job->workload();
        $work = json_decode($workString);
        //var_dump($workString);
        if(!isset($work->id)){
                return false;
        }
        $redis->hset('deviceListStatus',$work->equipment_number, $workString);
}

