<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-6-17
 * Time: 下午1:56
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH',dirname(__DIR__).DS.'app');
define('LOG_PATH',dirname(__DIR__).DS.'bin'.DS.'log');


require_once 'define.php';
date_default_timezone_set('Asia/Shanghai');
Server\Start::run();