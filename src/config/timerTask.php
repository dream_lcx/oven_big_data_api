<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */
/**
 * timerTask定时任务
 * （选填）task名称 task_name
 * （选填）model名称 model_name  task或者model必须有一个优先匹配task
 * （必填）执行task的方法 method_name
 * （选填）执行开始时间 start_time,end_time) 格式： Y-m-d H:i:s 没有代表一直执行,一旦end_time设置后会进入1天一轮回的模式
 * （必填）执行间隔 interval_time 单位： 秒
 * （选填）最大执行次数 max_exec，默认不限次数
 * （选填）是否立即执行 delay，默认为false立即执行
 */
$config['timerTask'] = [];
//下面例子表示在每天的14点到20点间每隔1秒执行一次
/*$config['timerTask'][] = [
    //'start_time' => 'Y-m-d 19:00:00',
    //'end_time' => 'Y-m-d 20:00:00',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
];*/
//下面例子表示在每天的14点到15点间每隔1秒执行一次，一共执行5次
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 14:00:00',
    'end_time' => 'Y-m-d 15:00:00',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
    'max_exec' => 5,
];*/
//下面例子表示在每天的0点执行1次(间隔86400秒为1天)
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 23:59:59',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '86400',
];*/
//下面例子表示在每天的0点执行1次
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 14:53:10',
    'end_time' => 'Y-m-d 14:54:11',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
    'max_exec' => 1,
];*/

////定时删除已经下载了的excel，每天的0点执行1次，间隔1小时
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 00:00:00',
//    'end_time' => 'Y-m-d 23:59:59',
//    'task_name' => 'AppTask',
//    'method_name' => 'testTask',
//    'interval_time' => '600',
//];
////定时删除core文件,一个小时执行一次
//$config['timerTask'][] = [
//    'task_name' => 'AppTask',
//    'method_name' => 'delSwooleCore',
//    'interval_time' => '1800',
//];
////定时删除core文件,一个小时执行一次
//$config['timerTask'][] = [
//    'task_name' => 'AppTask',
//    'method_name' => 'delcore',
//    'interval_time' => '1800',
//];
////优惠券过期
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 23:50:00',
//    'task_name' => 'AppTask',
//    'method_name' => 'couponVaild',
//    'interval_time' => '86400',
//];
//定时检测是否有消息要推送（废除）
//$config['timerTask'][] = [
//    'task_name' => 'ShowDataTask',
//    'method_name' => 'showData',
//    'interval_time' => '1',
//];

//检测主从数据库是否一致，关闭有数据不一致的风险
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 23:59:59',
//    'task_name' => 'mysqlSyncTask',
//    'method_name' => 'execCheckCommand',
//    'interval_time' => '86400',
//];

//离线设备队列
//$config['timerTask'][] = [
//    'task_name' => 'DevOffineQueueTask',
//    'method_name' => 'devnQueue',
//    'interval_time' => '2',
//];

//离线设备微信推送
//$config['timerTask'][] = [
//    'task_name' => 'DevOffineQueueTask',
//    'method_name' => 'wxPush',
//    'interval_time' => '2',
//];

//离线设备微信推送
//$config['timerTask'][] = [
//    'task_name' => 'DevOffineQueueTask',
//    'method_name' => 'on_line_push_mysql',
//    'start_time' => 'Y-m-d 23:59:59',
//    'interval_time' => '86400',
//];

//订单退款
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 01:00:00',
//    'end_time' => 'Y-m-d 03:59:59',
//    'task_name' => 'OrderTask',
//    'method_name' => 'refundTask',
//    'interval_time' => '3600',
//
//];
////订单分表
//$config['timerTask'][] = [
////    'start_time' => 'Y-m-d 02:00:00',
//    'task_name' => 'OrderTask',
//    'method_name' => 'subSaleOrderTable',
//    'interval_time' => '600',
//
//];
//同步在线离线
//$config['timerTask'][] = [
//    'task_name' => 'DeviceTask',
//    'method_name' => 'sysnStatus',
//    'interval_time' => '300',
//];
////水卡阈值提醒
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 15:00:00',
//    'task_name' => 'AppTask',
//    'method_name' => 'cardBalanceWarnning',
//    'interval_time' => '86400',
//];
////水卡低余额达到一定比例给经营者发送通知
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 17:00:00',
//    'task_name' => 'AppTask',
//    'method_name' => 'cardLowBalanceratioWarnning',
//    'interval_time' => '86400',
//];
////总后台统计
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 02:00:00',
//    'end_time' => 'Y-m-d 04:00:00',
//    'task_name' => 'StatisticsTask',
//    'method_name' => 'summaryStatistics',
//    'interval_time' => '3600',
//];
////合伙人后台统计
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 02:00:00',
//    'end_time' => 'Y-m-d 04:00:00',
//    'task_name' => 'StatisticsTask',
//    'method_name' => 'partnerSummaryStatistics',
//    'interval_time' => '3600',
//];

return $config;
