<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */

use Server\CoreBase\PortManager;


$config['ports'][] = [
    'socket_type' => PortManager::SOCK_HTTP,
    'socket_name' => '0.0.0.0',
    'socket_port' => 8482,
    'route_tool' => 'NormalRoute',
    'middlewares' => ['MonitorMiddleware','NormalHttpMiddleware','AuthHttpMiddleware'],
    'method_prefix' => 'http_'
];

$config['ports'][] = [
    'socket_type' => PortManager::SOCK_WS,
    'socket_name' => '0.0.0.0',
    'socket_port' => 8483,
    'route_tool' => 'WsRoute',
    'pack_tool' => 'NonJsonPack',
    'middlewares' => ['MonitorMiddleware'],
    'opcode' => PortManager::WEBSOCKET_OPCODE_TEXT,
    'event_controller_name' => 'TestController',
    'connect_method_name' => "onConnect",
];

return $config;
