<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/5 0005
 * Time: 下午 3:59
 */

$config['iot']['route'] = [
    //终端主动上报指令对应控制器与方法
    'l0'=>['c'=>'Water/Link','a'=>'maintain_link'],
    //机器状态
    'b1'=>['c'=>'Water/Iot','a'=>'machine_state'],
    //离线消费信息
    'b2'=>['c'=>'Water/Iot','a'=>'offline_consumer_information'],
    //消费信息
    'b3'=>['c'=>'Water/Iot','a'=>'consumption_information'],
    //报警信息
    'b4'=>['c'=>'Water/Iot','a'=>'alarm_information'],
    //系统参数
    'b5'=>['c'=>'Water/Iot','a'=>'system_parameter'],
    //售水参数 bg
    'b6'=>['c'=>'Water/Iot','a'=>'water_selling_parameters'],
    //制水参数
    'b7'=>['c'=>'Water/Iot','a'=>'water_making_parameters'],
    //控制参数
    'b8'=>['c'=>'Water/Iot','a'=>'control_parameters'],
    //上报实时卡余额参数指令（当本机没有卡数据时使用）
    'b9'=>['c'=>'Water/Iot','a'=>'card_balance_parameter'],
    'ba'=>['c'=>'Water/Iot','a'=>'card_balance_parameter_ba_reply'],
    //上报离线卡余额参数指令（当本机有卡数据时使用）
    'bb'=>['c'=>'Water/Iot','a'=>'offline_card_balance_parameter'],
    'bc'=>['c'=>'Water/Iot','a'=>'offline_card_balance_parameter_bb_reply'],



    //平台下发指令后终端上报对应控制器与方法
    //下发用户卡数据到设备
    'c1'=>['c'=>'Water/Iot','a'=>'issue_user_card_data'],
    //清除设备用户卡数据
    'c2'=>['c'=>'Water/Iot','a'=>'clear_user_card_data'],
    //平台控制器开关
    'c4'=>['c'=>'Water/Iot','a'=>'switch_control'],
    //下发打水
    'c5'=>['c'=>'Water/Iot','a'=>'recharge_payment'],
    //用户未打水或者水量未使用完，需要进行退款操作
    'c6'=>['c'=>'Water/Iot','a'=>'recharge_payment_unfinished'],
    //挂失、解挂、注销、解销卡
    'c7'=>['c'=>'Water/Iot','a'=>'hanging_loss'],
    //设置系统参数
    'c8'=>['c'=>'Water/Iot','a'=>'setting_system_parameter_reply'],
    //设置售水参数
    'c9'=>['c'=>'Water/Iot','a'=>'setting_water_selling_parameters_reply'],
    //设置制水参数
    'ca'=>['c'=>'Water/Iot','a'=>'setting_water_making_parameters_reply'],
    //设置控制参数
    'cb'=>['c'=>'Water/Iot','a'=>'setting_control_parameters_reply'],
    //设置域名或者IP
    'cc'=>['c'=>'Water/Iot','a'=>'setup_connection'],
    //未注册设备
    'ce'=>['c'=>'Water/Iot','a'=>'unregistered_equipment'],
    //设置主板密钥
    'd1'=>['c'=>'Water/Iot','a'=>'set_motherboard_key'],
    //设置主板兼容密钥
    'd3'=>['c'=>'Water/Iot','a'=>'set_motherboard_other_key'],
    //获取版本号
    'd4'=>['c'=>'Water/Iot','a'=>'get_version'],
    //远程恢复出厂设置
    'd5'=>['c'=>'Water/Iot','a'=>'restore_factory_settings'],
    //远程重启
    'd6'=>['c'=>'Water/Iot','a'=>'restart'],
    //设备初始化
    'ee'=>['c'=>'Water/Iot','a'=>'device_initialed'],
    //设备远程升级
    'ff'=>['c'=>'Water/IotIns','a'=>'device_upgrade'],
    '1f'=>['c'=>'Water/IotIns','a'=>'upgrade_request'],
    '2f'=>['c'=>'Water/IotIns','a'=>'firmware_process'],
    '3f'=>['c'=>'Water/IotIns','a'=>'firmware_end'],
    'dd'=>['c'=>'Water/Iot','a'=>'getIccid'],
];

return $config;