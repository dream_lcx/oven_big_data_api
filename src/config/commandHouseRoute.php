<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/5 0005
 * Time: 下午 3:59
 */

$config['commandhouse']['route'] = [
    //终端上报指令对应控制器与方法

    //机器状态
    '02'=>['c'=>'Admin/AreportController','a'=>'machine_state'],
    '01'=>['c'=>'House/Bind','a'=>'machine_state'],
    '03'=>['c'=>'House/Bind','a'=>'gj'],
    '04'=>['c'=>'House/Bind','a'=>'gj'],
    //消费信息
    'b2'=>['c'=>'Report','a'=>'consumption_information'],
    //离线消费信息
    'b3'=>['c'=>'Report','a'=>'offline_consumer_information'],
    //报警信息
    'b4'=>['c'=>'Report','a'=>'alarm_information'],
    //系统参数
    'b5'=>['c'=>'Report','a'=>'system_parameter'],
    //售水参数
    'b6'=>['c'=>'Report','a'=>'water_selling_parameters'],
    //制水参数
    'b7'=>['c'=>'Report','a'=>'water_making_parameters'],
    //控制参数
    'b8'=>['c'=>'Report','a'=>'control_parameters'],
    //运行参数
    'b9'=>['c'=>'Report','a'=>'operating_parameters'],


    //平台下发指令对应控制器与方法
    //查询机器状态
    'c1'=>['c'=>'Water','a'=>'query_machine_state'],
    //开关控制
    'c2'=>['c'=>'Water','a'=>'switch_control'],
    //充值支付
    'c3'=>['c'=>'Water','a'=>'recharge_payment'],
    //挂失、解挂
    'c4'=>['c'=>'Water','a'=>'hanging_loss'],
    //设置系统参数
    'c5'=>['c'=>'Water','a'=>'setting_system_parameters'],
    //设置售水参数
    'c6'=>['c'=>'Water','a'=>'setting_water_selling_parameters'],
    //设置制水参数
    'c7'=>['c'=>'Water','a'=>'setting_water_parameters'],
    //设置控制参数
    'c8'=>['c'=>'Water','a'=>'setting_control_parameters'],
    //设置运行参数
    'c9'=>['c'=>'Water','a'=>'setting_running_parameters'],
    //置域名或IP
    'ca'=>['c'=>'Water','a'=>'setup_connection'],
    //未注册设备
    'cf'=>['c'=>'Water','a'=>'unregistered_equipment'],
];

return $config;