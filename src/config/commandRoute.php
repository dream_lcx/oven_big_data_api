<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/5 0005
 * Time: 下午 3:59
 */

$config['command']['route'] = [
    //终端上报指令对应控制器与方法
    
    //机器状态
    'b1'=>['c'=>'Water/Report','a'=>'machine_state'],
    //消费信息
    'b2'=>['c'=>'Water/Report','a'=>'consumption_information'],
    //离线消费信息
    'b3'=>['c'=>'Water/Report','a'=>'offline_consumer_information'],
    //报警信息
    'b4'=>['c'=>'Water/Report','a'=>'alarm_information'],
    //系统参数
    'b5'=>['c'=>'Water/Report','a'=>'system_parameter'],
    //售水参数 bg
    'b6'=>['c'=>'Water/Report','a'=>'water_selling_parameters'],
    //制水参数
    'b7'=>['c'=>'Water/Report','a'=>'water_making_parameters'],
    //控制参数
    'b8'=>['c'=>'Water/Report','a'=>'control_parameters'],
    //运行参数 bg
    'b9'=>['c'=>'Water/Report','a'=>'operating_parameters'],
    //上传卡余额参数指令
    'ba'=>['c'=>'Water/Report','a'=>'card_balance_parameter'],
    'bb'=>['c'=>'Water/Report','a'=>'card_balance_parameter_ba_reply'],


    //平台下发指令对应控制器与方法
    //查询机器状态
    'c1'=>['c'=>'Water/Water','a'=>'query_machine_state'],
    //开关控制
    'c2'=>['c'=>'Water/Water','a'=>'switch_control'],
    //充值支付 bg
    'c3'=>['c'=>'Water/Water','a'=>'recharge_payment'],
    //挂失
    'd8'=>['c'=>'Water/Water','a'=>'hanging_loss'],
    //解挂
    'c4'=>['c'=>'Water/Water','a'=>'solution_loss'],
    //设置系统参数
    'c5'=>['c'=>'Water/Water','a'=>'setting_system_parameters'],
    //设置售水参数
    'c6'=>['c'=>'Water/Water','a'=>'setting_water_selling_parameters'],
    //设置制水参数
    'c7'=>['c'=>'Water/Water','a'=>'setting_water_parameters'],
    //设置控制参数 bg
    'c8'=>['c'=>'Water/Water','a'=>'setting_control_parameters'],
    //设置运行参数
    'c9'=>['c'=>'Water/Water','a'=>'setting_running_parameters'],
    //置域名或IP  bg
    'ca'=>['c'=>'Water/Water','a'=>'setup_connection'],
    //未注册设备
    'cf'=>['c'=>'Water/Water','a'=>'unregistered_equipment'],
];

return $config;