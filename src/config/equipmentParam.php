<?php
/**
 * 设备参数配置
 */
//控制参数
$config['equipmentParam']['control'] = [
    '00' => [//原类型售水机宏卡版U8H
        ['id' => '01', 'name' => '保温开关','display'=>true, 'unit' => '', 'type' => 'radio', 'default_value' => '00', 'option' => ['00' => '关闭', '01' => '开启']],
        ['id' => '02', 'name' => 'TDS1检测开关','display'=>true, 'unit' => '', 'type' => 'radio', 'default_value' => '00', 'option' => ['00' => '关闭', '01' => '开启']],
        ['id' => '03', 'name' => 'TDS1设定值','display'=>true, 'unit' => '', 'type' => 'input', 'default_value' => '195','max_value'=>'999','min_value'=>'1'],
        ['id' => '04', 'name' => 'TDS2检测开关','display'=>true, 'unit' => '', 'type' => 'radio', 'default_value' => '00', 'option' => ['00' => '关闭', '01' => '开启']],
        ['id' => '05', 'name' => 'TDS2设定值','display'=>true, 'unit' => '', 'type' => 'input', 'default_value' => '195','max_value'=>'999','min_value'=>'1'],
        ['id' => '06', 'name' => '心跳间隔时间','display'=>true, 'unit' => '秒', 'type' => 'input', 'default_value' => '10','max_value'=>'600','min_value'=>'10'],
        ['id' => '07', 'name' => '断网重连时间','display'=>true, 'unit' => '秒', 'type' => 'input', 'default_value' => '10','max_value'=>'60','min_value'=>'5'],
        ['id' => '08', 'name' => '流量监测','display'=>true, 'unit' => '', 'type' => 'radio', 'default_value' => '01', 'option' => ['00' => '流量模式', '01' => '时间模式']],
    ],
    '01' => [//新类型售水机4G单出水版U8G
        ['id' => '01', 'name' => '保温开关','display'=>true, 'unit' => '','type'=>'radio','default_value' => '00', 'option'=>['00'=>'关闭','01'=>'开启']],
        ['id' => '02', 'name' => 'TDS1检测开关','display'=>true, 'unit' => '','type'=>'radio','default_value' => '00', 'option'=>['00'=>'关闭','01'=>'开启']],
        ['id' => '03', 'name' => 'TDS1设定值','display'=>true, 'unit' => '','type'=>'input','default_value' => '195','max_value'=>'999','min_value'=>'1'],
        ['id' => '04', 'name' => 'TDS2检测开关','display'=>true, 'unit' => '','type'=>'radio','default_value' => '00','option'=>['00'=>'关闭','01'=>'开启']],
        ['id' => '05', 'name' => 'TDS2设定值','display'=>true, 'unit' => '','type'=>'input','default_value' => '195','max_value'=>'999','min_value'=>'1'],
        ['id' => '06', 'name' => '心跳间隔时间','display'=>true, 'unit' => '秒','type'=>'input', 'default_value' => '30','max_value'=>'600','min_value'=>'10'],
        ['id' => '07', 'name' => '断网重连时间','display'=>true, 'unit' => '秒','type'=>'input', 'default_value' => '10','max_value'=>'60','min_value'=>'5'],
        ['id' => '08', 'name' => '计费模式','display'=>true, 'unit' => '','type'=>'radio','default_value' => '01','option'=>['00'=>'流量模式','01'=>'时间模式']],
    ]
];
//系统参数sys
$config['equipmentParam']['sys'] = [
    '00' => [//原类型售水机宏卡版U8H
        ['id' => '01', 'name' => 'Ro膜冲洗时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'18','max_value'=>'240','min_value'=>'1'],
        ['id' => '02', 'name' => 'RO膜冲洗间隔','display'=>true, 'unit' => '小时', 'type' => 'input','default_value'=>'2','max_value'=>'10','min_value'=>'1'],
        ['id' => '03', 'name' => '投币器设置','display'=>true, 'unit' => '', 'type' => 'radio','default_value'=>'02', 'option' => ['01' => '一元投币器', '02' => '一元、五角投币器']],
        ['id' => '04', 'name' => '设置设备时间','display'=>true, 'unit' => '', 'type' => 'date','default_value'=>'']
    ],
    '01' => [//新类型售水机4G单出水版U8G
        ['id' => '01', 'name' => 'Ro膜冲洗时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'18','max_value'=>'240','min_value'=>'1'],
        ['id' => '02', 'name' => 'RO膜冲洗间隔','display'=>true, 'unit' => '小时', 'type' => 'input','default_value'=>'2','max_value'=>'10','min_value'=>'1'],
        ['id' => '03', 'name' => '投币器设置','display'=>false, 'unit' => '', 'type' => 'radio','default_value'=>'02', 'option' => ['01' => '一元投币器', '02' => '一元、五角投币器']],//硬件暂未支持该功能,跟李庆确认先隐藏此设置
        ['id' => '04', 'name' => '设置设备时间','display'=>true, 'unit' => '', 'type' => 'date','default_value'=>'']
    ]
];

//售水参数sale_water
$config['equipmentParam']['sale_water'] = [
    '00' => [//原类型售水机宏卡版U8H
        ['id' => '01', 'name' => '刷卡预扣费','display'=>true, 'unit' => '分', 'type' => 'input','default_value'=>'100','max_value'=>'900','min_value'=>'50'],
        ['id' => '02', 'name' => '脉冲数设置','display'=>true, 'unit' => '脉冲', 'type' => 'input','default_value'=>'50','max_value'=>'500','min_value'=>'1'],
        ['id' => '03', 'name' => '刷卡取水时长','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'60','max_value'=>'240','min_value'=>'1'],
        ['id' => '04', 'name' => '投币个数设置','display'=>true, 'unit' => '个', 'type' => 'input','default_value'=>'5','max_value'=>'10','min_value'=>'1'],
        ['id' => '05', 'name' => '投币取水时长','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'7500','max_value'=>'20000','min_value'=>'100'],
        ['id' => '06', 'name' => '每秒显示出水量','display'=>true, 'unit' => '毫升', 'type' => 'input','default_value'=>'1','max_value'=>'5','min_value'=>'1'],
        ['id' => '07', 'name' => '金额回收时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'30','max_value'=>'240','min_value'=>'1'],
        ['id' => '08', 'name' => '打水灯延时关闭','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'15','max_value'=>'240','min_value'=>'1'],
        ['id' => '09', 'name' => '取水门锁延时关闭时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'15','max_value'=>'240','min_value'=>'1']

    ],
    '01' => [//新类型售水机4G单出水版U8G
        ['id' => '01', 'name' => '刷卡预扣费','display'=>true, 'unit' => '分', 'type' => 'input','default_value'=>'250','max_value'=>'900','min_value'=>'50'],
        ['id' => '03', 'name' => '预扣费取水时长','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'60','max_value'=>'240','min_value'=>'1'],
        ['id' => '05', 'name' => '预扣费取水量','display'=>true, 'unit' => '毫升', 'type' => 'input','default_value'=>'7500','max_value'=>'20000','min_value'=>'100'],
        ['id' => '10', 'name' => '预设投币金额','display'=>true, 'unit' => '元', 'type' => 'input','default_value'=>'2.5','max_value'=>'120','min_value'=>'0.5'],
        ['id' => '11', 'name' => '预设投币取水量','display'=>true, 'unit' => '毫升', 'type' => 'input','default_value'=>'7500','max_value'=>'60000','min_value'=>'1'],
        ['id' => '12', 'name' => '预设投币取水时长','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'60','max_value'=>'240','min_value'=>'1'],
        //['id' => '04', 'name' => '投币个数设置', 'unit' => '个', 'type' => 'input','default_value'=>'5'],
        ['id' => '04', 'name' => '投币金额上限','display'=>true, 'unit' => '元', 'type' => 'input','default_value'=>'2.5','max_value'=>'5','min_value'=>'0.5'],//与协议存在差异,协议是下发投币个数,由于客户不清楚每个投币多少钱,故系统设置投币金额（每个投币价值0.5元）
        ['id' => '02', 'name' => '脉冲数设置','display'=>true, 'unit' => '脉冲', 'type' => 'input','default_value'=>'50','max_value'=>'500','min_value'=>'1'],
//        ['id' => '06', 'name' => '每秒显示出水量', 'unit' => '毫升','type'=>'input'],
        ['id' => '07', 'name' => '金额回收时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'30','max_value'=>'240','min_value'=>'15'],
        ['id' => '08', 'name' => '打水灯延时关闭','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'15','max_value'=>'240','min_value'=>'5'],
        ['id' => '09', 'name' => '取水门锁延时关闭时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'15','max_value'=>'240','min_value'=>'5'],

    ]
];
//制水参数system_water
$config['equipmentParam']['system_water'] = [
    '00' => [//原类型售水机宏卡版U8H
        ['id' => '01', 'name' => '制水开启延时','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'1','max_value'=>'5','min_value'=>'1'],
        ['id' => '02', 'name' => '冲洗时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'10','max_value'=>'240','min_value'=>'5'],
        ['id' => '03', 'name' => '冲洗时间间隔','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'120','max_value'=>'240','min_value'=>'30'],
        ['id' => '04', 'name' => '臭氧杀菌时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'30','max_value'=>'240','min_value'=>'1'],
        ['id' => '05', 'name' => '臭氧杀菌间隔','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'120','max_value'=>'240','min_value'=>'1'],
        ['id' => '06', 'name' => '加热温度下限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'5','max_value'=>'35','min_value'=>'5'],
        ['id' => '07', 'name' => '加热温度上限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'10','max_value'=>'35','min_value'=>'10'],
        ['id' => '08', 'name' => '广告灯开启时间','display'=>true, 'unit' => '', 'type' => 'time','default_value'=>'1930','max_value'=>'2359','min_value'=>'0000'],
        ['id' => '09', 'name' => '广告灯关闭时间','display'=>true, 'unit' => '', 'type' => 'time','default_value'=>'2330','max_value'=>'2359','min_value'=>'0000']

    ],
    '01' => [//新类型售水机4G单出水版U8G
        ['id' => '01', 'name' => '制水开启延时','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'1','max_value'=>'5','min_value'=>'1'],
        ['id' => '02', 'name' => '冲洗时间', 'display'=>true,'unit' => '秒', 'type' => 'input','default_value'=>'10','max_value'=>'240','min_value'=>'5'],
        ['id' => '03', 'name' => '冲洗时间间隔','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'120','max_value'=>'240','min_value'=>'30'],
        ['id' => '04', 'name' => '臭氧杀菌时间','display'=>true, 'unit' => '秒', 'type' => 'input','default_value'=>'30','max_value'=>'240','min_value'=>'1'],
        ['id' => '05', 'name' => '臭氧杀菌间隔','display'=>true, 'unit' => '分钟', 'type' => 'input','default_value'=>'120','max_value'=>'240','min_value'=>'1'],
        ['id' => '06', 'name' => '加热温度下限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'5','max_value'=>'35','min_value'=>'5'],
        ['id' => '07', 'name' => '加热温度上限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'10','max_value'=>'35','min_value'=>'10'],
        ['id' => '08', 'name' => '广告灯开启时间','display'=>true, 'unit' => '', 'type' => 'time','default_value'=>'1930','max_value'=>'2359','min_value'=>'0000'],
        ['id' => '09', 'name' => '广告灯关闭时间','display'=>true, 'unit' => '', 'type' => 'time','default_value'=>'2330','max_value'=>'2359','min_value'=>'0000'],
        ['id' => '10', 'name' => '机箱温度下限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'5','max_value'=>'35','min_value'=>'5'],
        ['id' => '11', 'name' => '机箱温度上限','display'=>true, 'unit' => '摄氏度', 'type' => 'input','default_value'=>'50','max_value'=>'50','min_value'=>'35'],

    ]
];

//运行参数run
$config['equipmentParam']['run'] = [
    '00' => [//原类型售水机宏卡版U8H
        ['id' => '01', 'name' => '清空累计金额', 'display'=>true,'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>''],
        ['id' => '02', 'name' => '初始化','display'=>true, 'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>''],
        ['id' => '03', 'name' => '清空名单','display'=>true, 'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>'']

    ],
    '01' => [//新类型售水机4G单出水版U8G
        ['id' => '01', 'name' => '清空累计金额','display'=>true, 'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>''],
        ['id' => '02', 'name' => '初始化','display'=>true, 'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>''],
        ['id' => '03', 'name' => '清空名单','display'=>true, 'unit' => '', 'type' => 'input','default_value'=>'00','max_value'=>'','min_value'=>'']
    ]
];

return $config;