<?php

/*
 * 自定义配置文件
 */
//拦截器
$config['custom']['interceptor'] = [
    'User' => [
        'Login' => [
            'index',
            'login_ali',
            'login_ali_phone',
            'register',
            'send_sms',
            'dologin'
        ],
        'EqStatus' => [
            'getStatus',
            'getWaterConf'
        ],
        //回调控制器
        'Callback' => [
            'order_call',
            'order_call_ali',
            'waterCardCommon',
            'waterCardCommonAli',
            'waterCardDeposit',
            'waterCardDepositAli',
            'rechargeOrderDeposit',
            'waterCardCommonTest'
        ],
        'Wx' => [
            'getUserOpenid',
            'aesPhoneDecrypt',
        ],
        //by gj 2020/2/18
        'WaterStation' => [
            'getEquipmentVillage',
            'operationlist',
            'searchDevice',
        ],
        'Article' => [
            'articleList',
            'articleDetails',
            'aboutUs',
        ],
        'Order' => [
            'alipayRefund',
        ]
    ],
    'Manage' => [
        'User' => [
            'dologin',
            'getCodeToToken',
            'retrievePassword'
        ]
    ],
    'Partner' => [
        'User' => [
            'dologin',
            'retrievePassword',
            'getUserOpenid',
            'userSyn'
        ],
        'Callback' => [
            'ali_scan_pay',
            'wx_scan_pay',
            'wxXcxPay'
        ],
        'Setup' => [
            'getBanks'
        ],
        'Equipment' => [
            'batch_create_qr_code',
            'create_qr_code'
        ]
    ],
    'Salesman' => [
        'Login' => [
            'login',
            'log_out',
            'retrievePassword',
            'getUserOpenid'
        ],
        'Callback' => [
            'salesmanRecharge'
        ]
    ]
];
$config['token_key'] = 'water_user_key';
//微信配置
if (DEBUG) {
    $config['wx']['app_wx_name'] = '滳滳打水-深鲜PLUS';
    $config['wx']['appId'] = 'wx94c733a992c22a22';
    $config['wx']['appSecret'] = 'e7ce048cf22396181df305de594ef4cf';
    $config['wx']['pay_key'] = 'chongqingchuanjinguanggaoyouxian';
    $config['wx']['mchid'] = '1414355802';
    //经营助手
    $config['s_wx'] = [
        'app_wx_name' => '促销员-妈妈米虾',
        'appId' => 'wx2845e1b4ffe09c17',
        'appSecret' => '01dc478944f774b68185f9fb903e1f32',
        'pay_key' => 'chongqingyouheyizhongwangluokeji',
        'mchid' => '1486380572',
    ];


} else {
    //用户端
    $config['wx']['app_wx_name'] = '滳滳打水';
    $config['wx']['appId'] = 'wxe3989937410512f8';
    $config['wx']['appSecret'] = '8e99493cf5b367290efe3399dcd457b6';
    $config['wx']['pay_key'] = 'chongqingyouheyizhongwangluokeji';
    $config['wx']['mchid'] = '1500484712';
    //经营助手
    $config['s_wx'] = [
        'app_wx_name' => '促销员',
        'appId' => 'wxc3f5aa4ce7d6f70e',
        'appSecret' => 'f811cc24742a89e8780a1bd165e987ff',
        'pay_key' => 'chongqingyouheyizhongwangluokeji',
        'mchid' => '1500484712',
    ];

}
//支付宝配置
$config['ali'] = [
    'app_wx_name' => '滳滳打水',
    'appId' => '2019110468881302',
    'encryptKey' => '58nymo9OQbEZ/llsj2klmw==',

    'rsaPrivateKey' => 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBoxpYSAc2yjHbHkQGyiQGLB6szd9rrqfjglxhDgNTtAmGQhOT1LeENPCQsEkJRQyHSIssQPg19wIqe+T1ayh+XkwYaOBO2d1wfp57oiK7nzzypMphUcKtiq/AAOIEZQJTTerBPDZFuGJbR0y3WGUZb+UI7qaQaGQJpTwHj/NnmUlSDkQ6l0jY3eEhqOjIiGgiZWtDR40GuuhCkWxtDoA/BDrlFW2JN16qGgudnBdw5i7vomCLfRIDzbK4sreYgOStoviwk5bUMsjUgIIdNw+g7P4Kqc4kznE10h1/2qT8u0wL+0nbQ3bYBt3BfFbJtYLS1l1zbRE52PtzKcclN6+fAgMBAAECggEAV+lepT17PumRM7Hrmdsc4N6skq0t9N1FVyCBOwQAt5Og3d4TXq3jIZGxDb2uWc3seBV247TPXfE7E8pe8xsPI9sYqXj9nBtccrw7B0ZlXWzYLQgZgjOq3HrvQjqXCznggNOW/iGyAEfQ02ufl7onHT/nRs5oQAe6x1u5QLua6cZvfvAPydGlXJWBo1irc9A0oNygdFde4tS4Qe7e2AJ2JCY07f18+CuFza0yI9IbaHkZfrt67FUF5z26eBzEea3uRguoMsZhMaiduufnNYAifGdn9LyUHKsay1/TFC3T6dwfIqRwdehCXSAw2dUePiGyyTWnm6eD8fiVgBivxlBzyQKBgQC3mOZ+Xpoie9WA7+OruZIewUIBCzqsHLXByoiNgbbVJm/d31i3QVFtW4aG9bQ2Z6nJRBKoZKdc/aHvvu5N83jDYHQe8AHSjvZbM8xnJ9hTXreq6TMZVqeXMXDQUKp1tcLdEJ63vLWeEfIxqrG/Y7K68zWdlzJsKBjIDVe74I+VSwKBgQC0wqUayE8/rxHvt1GRwOJ7Bq9FO6PRNHeQecGW+aqRbkz3JA5ummGaOqJtNjogoA1OzCQjaE+nmfHlfdpMScVRZLgzaydtlFSMtmSLgSOXAR/jlXiFh64txfKWjUOed8mvEjKLOml2wBwpHR87LStvxxwrFCAO3qG/7NOg+bcefQKBgQC01zWKGdLGzqUxeJkP6BymkINcFdx39VuOg9lXUQJvF4ouyDVRZFQ5XGK5AsG8qQUTSyr5lRNJREbCqJWAUKaOA669XnjdFpZQpUfb/+JT1EMjZMpBVhqDkV5mwl30zdEgAKPlpUpbZZ/jDhhAxYoiHTedf3VaFXC7dHELjRjA2wKBgAKDPiZMKIXbGiW556FeDAfnxOHyfUl4MD1K4/0ERXG2Fc3Nq4GwFbE+JWndOhuf/e9deRcg6RKMWkd1Y2ZyFBMYklLsp2SnjjFSi71As0zzEA1q030PsMUkHkYneowmYCBV4Wv8COFEtF+B7yA0dYkdPHAA3J04TQlM8ngnYHtFAoGAcO1dkjzbmvZ8SG9u/vjkymgb4X2/x13u936eeXsgl4fTtvznxm7LKrCvsySQdXKr+Df9S8rkhk++Tt/h3Mw6ZAkXTZa0MuhkjaL8ljf2QJF9icI09BSsX9PvOPS9kDWgG90NuZ4owQ7KMawmHz0PoEnWikhlL6KoUDmQHI2tk68=',

    'alipayrsaPublicKey' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyTiRa6bNwBjsainVhQRjhx/VW4Dr9tjHbMKaVBytCmmEtIEhbeLq9ix02WJoYyD4BOPtUGI1Jlq/eA4kscC5dSJSuL7pJytNfnSgkUq5MpboF/f6aC7AYZtEmb9OooaLA9ZXav/RqMTGt1Q5KbY4YZDZs/d28cb516Rl8x6XOMNklDYDWhzOA0oMadXN74lp5OMNo7z/ISbEMGAOczz2BIxHnbYUSA5rPuGVFwApNERc5MeAVYqojXjJpDztp24LLGjvjC3WJmPpDrwJMvcA2Z92V9uGHNPzAF+KoKzxHRzRaFhVwRohBFSmP1EupIud4RLxbzRenQGWzfOGgOQHEwIDAQAB',
];


//用户端：微信回调域名
if (DEBUG) {
    $config['equipment_addr'] = 'https://water.wechat-app.me';
    $config['callback_domain_name'] = 'https://waterapi.wechat-app.me';
    //二维码地址
    $config['qr_addr'] = 'https://waterapi.wechat-app.me';
    //后台地址
    $config['admin_addr'] = 'http://wadmin.wechat-app.me';
} else {
    $config['equipment_addr'] = 'https://js.youheone.com';
    $config['callback_domain_name'] = 'https://water.youheone.com';
    $config['qr_addr'] = 'https://water.youheone.com';
    //后台地址
    $config['admin_addr'] = 'http://home.youheone.com';
}

//七牛云地址
$config['qin_niu'] = 'https://qiniu.youheone.cn/';
//八米 短信账号
$config['sms']['uid'] = '1036';
$config['sms']['passwd'] = 'weipaitan123456';
$config['sms']['content'] = '智能水站';


//合伙人充值金额配置
$config['recharge']['money'] = [
    '1' => 100,
    '2' => 200,
    '3' => 300,
    '4' => 500,
    '5' => 1000,
    '6' => 1500,
    '7' => 2000,
    '8' => 3000,
];
//合伙人二微路径
$config['recharge']['qr_pat'] = '/partner';
$config['alipay'] = [
    //应用ID,您的APPID。
    'app_id' => "2016091500519721",
    //商户私钥 不能有空格 回车 否则签名错误
    'merchant_private_key' => "MIIEpQIBAAKCAQEAvfYjH4pmAiBgiAvRThiRka0ijfAFAgE75DTGMmBwfCSo9ukUqHtNB0zIgCuDK4H7gJMCl7jsbqwv1mIZjYrcD4+p3URaDQkPp41XiMJ4fBYG6cExlmp8iES5wUW2yNHDBX/Rbh6xzmgwjZYAK3gE5EQfgwBwVvsAAsh4WRTAl78xTqKsMw2nWEoERj/ybLk4oM9V9zl88lhVZG0rifrAD4YZgnBMuhzcgP6mJXs9t0PUl6vV42nsStdNv+Gde1+A3oVknAEXjCfoMUbbiT/a4Fr4TIznPm1q3FepKipsSIfTD87KU54lcWKi3j69AIWp1ETKq7/5lZ58Yl8uRwWuuQIDAQABAoIBAQCt/w9K65zmI7vnwVNVQkHL+pclBY3AuVqffAtyx00hGP8/Ml1+uXnjztv0MmbyahvxODbuvC6sbMFgj/ix4lgRVNkVWG9Xc05IfbmJBgAQQKjSYehGy4EH3FN9CZsUNLZ6igpN4IWi92JJvfSn3EY2w4NNnhBIS55F7lP13IFQO/Vm0eTtlytx8UXInKtgWPfBAkfG4h/5eQ46GEEfXtS8w+nQNCXiVRTowM6E28xZh9Kq5uNDhsq9Z+M2XSLOta+wJeBfN5yH/pnF8yGD+0mwCBre+MI+xDVPT29vLtfPWHWLOXt2UfHRUPLw4vLqE25Z9PbFGzD0dFfwFF8vsKaVAoGBAPJiG3UidmhPdSUr3fc+4eVtTKd1x4hSTW2JOFo63+DN5XiLGDQf59GXkkronO7mdA6YdAcLYivfp7X0EdYj20HVh7N0XMYvnSt57yDqbNaiz3lfE7VgOK/GX2QIP4SuQhSxaeW7x11uG9WugP/bACxLGWdQvmaeOdWX4jliAW7zAoGBAMiiHSNsmn/+E9Bgu+eZphxsJgeePpOCT0TCk3/cZDxTFpbtQ0R0COybtNCtiswnwN4nIMl0ti5vpwzp9xW2kpHAip0aLs7EpiAeFgJqIJzK4iLNpNvyXfpNV6/6w1ijs0jlsneq23476EIk3NU7ot8mpsgHnKkUclfvxbFN+k6jAoGBALdUoXQNpwXq465D6dXzCCXicBJRsaRYrBvnqGoHEgMVeTWd76atY0hSnuJt4xWH1cycX1Zcs9p/hnjHrR4NbO/0tHl10B5PMVALIGYDKmOGLeudkGil6Pp6zvf4jSKsM2s3bePIDlmKyR7sqMHdQdo+y24r0ngHwbLe+MtyZ6clAoGAVd8c/ZZ0IigLLvrYMe5PbIbVWBlgyx3Q86scGnOn6zHcz0hJrDjFwQ97un0slOjG4c55eAAeIY3kP+C7wm1cGkoAifE064a7W6/E18+4maM0WpUN5J29n24PA+EeTwfxqle1pQZZ35Ogev7wDcvyj/QpGiA36x43OnADeznW0mUCgYEAqpLqw1v2Q/PmiIwfNEvUNDLyFnaJQx5/hn6155aNljXd4LHowVhPD77H9v+XMfwdaPxvs+nWzxed18Z0Pz5xBAu2Z9aeqwzt5bN4LyRLB6ar+TTwR6X7xMYeQC4g0/O+jJ6Rpxx7sjWqAfffQj3qZedzb9LRNkXcye4eEnDX/fE=",
    //异步通知地址
    'notify_url' => $config['qr_addr'] . "/Partner/Callback/ali_scan_pay",
    //同步跳转
    'return_url' => $config['qr_addr'],
    //编码格式
    'charset' => "UTF-8",
    //签名方式
    'sign_type' => "RSA2",
    //支付宝网关
    'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do",
    //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。 不能有空格 回车 否则签名错误
    'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtVex9AygCEror+HFUebTMXRfKEDQp1XBBcTJrbBNJpxJ6XjCByz0dyIRWr3zM2c5ayoUXbCtqB7PdXr5+VAsNgVVSgp1P+R/cJhtdqI48Hod7atuWb/zV9fViTyknuJfBEiWSZG9uefZkKT1FiU6fipRnSJElkG8FIGpo5X4zSbHOds3rR1NSw2oe30l+TBJy5rAfSMnZZ1CU+/Pf11qT8wmiO/zqWs9XpOdKGeiTnaR/zqRVc0EiWFnQiD1DglS9QkQeO3YYmCD42l5mwDX2geimL+wuD8/pfI2e5UD7A9nvQfXUXxabz7i9KO03Z4drurJhKLOjAjKP8b/ZEGOcQIDAQAB",
];


$config['c_type'] = [1 => '普通卡', 2 => '老年卡', 3 => '婴幼儿卡', 4 => '贫困卡', 5 => '员工卡', 6 => '管理卡'];//水卡类型
$config['apply_c_type'] = [0 => '普通卡', 1 => '老年卡', 2 => '婴幼儿卡', 3 => '贫困卡',4 => '员工卡', 5 => '管理卡'];//申请水卡时水卡类型，由于原数据库设计申请表与水卡表类型不一致，冗余设置
$config['credential_type'] = [1 => '身份证', 2 => '军官证', 3 => '户口本', 4 => '出身证明', 5 => '贫困证明'];//证明材料类型{
$config['special_card_recharge_money'] = [0, 30, 50, 60, 100, 120];//特殊水卡充值金额配置
$config['pay_type'] = [1 => '微信支付', 2 => '实体卡支付', 3 => '虚拟卡支付', 4 => '积分支付', 5 => '投币', 6 => '支付宝支付'];//支付类型
$config['recharge_order_pay_status'] = [1 => '充值', 2 => '充值未支付', 3 => '充值赠送', 4 => '促销项目', 5 => '修改金额'];//充值订单支付状态/来源
$config['recharge_order_recharge_source'] = [1 => '用户充值', 2 => '合伙人充值', 3 => '平台充值', 4 => '加盟商充值', 5 => '销售员充值', 6 => '充值卡充值', 7 => '水卡合并'];//充值订单来源
//水卡资金记录来源配置
$config['card']['capital']['record'] = [
    '1' => '充值',
    '2' => '打水',
//    '3' => '平账',//水卡资金记录不显示平账记录
    '4' => '余额修改',
    '5' => '充值赠送',
    '6' => '促销活动赠送',
    '7' => '年审',
    '8' => '其他'
];
/**
 * 区域码秘钥配置
 * name 秘钥名称
 * value 秘钥
 * type 类型 1通用区域码的秘钥 2兼容区域码的秘钥
 * key A秘钥  key_b B秘钥
 */
$config['card_mother_key'] = [
    'key' => [
        [
            'name' => '优净云',
            'value' => '957007003105',
            'type' => 1
        ],
        [
            'name' => 'H2.0',
            'value' => '957008003506',
            'type' => 2
        ],
        [
            'name' => 'W1.0',
            'value' => '867607003905',
            'type' => 2
        ],

    ],
    'key_b' => [
        [
            'name' => '通用',
            'value' => 'befade120690',
            'type' => 1
        ],
        [
            'name' => '通用',
            // 'value' => 'befade120690',
            'value' => '900612defabe',
            'type' => 2
        ],
    ]
];
//区域码类型
$config['area_code_type'] = [1 => '通用', 2 => '兼容'];
//区域码下发状态
$config['area_code_send_status'] = [2 => '下发中', 1 => '设置成功', 3 => '设置失败'];
$config['card_operation_type'] = [1 => '水卡解绑', 2 => '水卡挂失', 3 => '水卡解除挂失', 4 => '水卡绑定', 5 => '水卡申请', 6 => '注销水卡', 7 => '解除注销', 8 => '水卡补卡',9=>'余额修改'];//操作类型
//设备联网方式
$config['device_networking_mode'] = [1 => '2G', 2 => '4G'];
//水卡状态
$config['water_card_status'] = [1 => '正常', 2 => '挂失', -1 => '注销'];
//提现账户类型
$config['withdraw_account_type'] = [1=>'银行卡',2=>'微信零钱',3=>'支付宝'];
//合伙人资金记录=业务类型
$config['capital_record_btype'] = [1=>'线上打水',2=>'用户水卡充值',3=>'合伙人水卡充值',4=>'水电费',5=>'耗材费',6=>'其他费用',7=>'积分兑换',8=>'加盟商账户充值',9=>'销售账户充值',10=>'其他'];

//华灶大数据平台消息通知配置
//预约通知
$config['order_notification'] = [
    'title'=>'预约通知',
    'content'=>'用户发起预约',
];
//安装通知
$config['installation_notification'] = [
    'title'=>'安装通知',
    'content'=>'新装一台设备',
];
//签署合同通知
$config['notice_of_contract'] = [
    'title'=>'签署合同通知',
    'content'=>'用户成功签署合同'
];
//鸡娃图默认经纬度
$config['lat'] = '30.65984';
$config['lng'] = '104.10194';
//颜色
$config['color'] = '#4ab2e5';
return $config;
