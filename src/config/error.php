<?php
//错误收集上报系统
$config['error']['enable'] = false;
//访问地址，需自己设置ip：port
//是否显示在http上
$config['error']['http_show'] = false;

//$config['error']['url'] = "Http://192.168.17.128:8481/Error";

$config['error']['url'] = "Http://114.115.207.148:8381/Error";

$config['error']['redis_prefix'] = '@sd-error_';
$config['error']['redis_timeOut'] = '36000';

$config['error']['dingding_enable'] = true;
$config['error']['dingding_url'] = 'https://oapi.dingtalk.com';
//钉钉机器人，需自己申请

$config['error']['dingding_robot'] = '/robot/send?access_token=a76672e52097bdec66a55d9224b183bda6797ec675224e40073ba30818bb306c';
return $config;
