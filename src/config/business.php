<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */
//强制关闭gzip
$config['http']['gzip_off'] = true;
//默认访问的页面
$config['http']['index'] = 'index.html';
/**
 * 设置域名和Root之间的映射关系
 */
$config['http']['root'] = [
    'default' =>
        [
//            'index' => ['TestController', 'test'] //转到控制器
            'root' => 'www',
            'index' => 'index.html' //转到指定页面
        ]
    ,
    '139.9.153.80' =>
        [
            'root' => 'www',
            'index' => 'index.html' //转到指定页面
        ],
    'waterapi.wechat-app.me' =>[
        'root' => 'partner',
        'index' => 'index.html' //转到指定页面
    ],
    'water.youheone.com' =>[
        'root' => 'partner',
        'index' => 'index.html' //转到指定页面
    ],

];
return $config;