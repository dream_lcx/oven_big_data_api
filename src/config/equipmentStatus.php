<?php
/****设备状态*********/
$config['equipmentStatus'] = [
    //设备状态
    'status' => [
        ['id' => 1, 'desc' => '正常', 'warn' => false],
        ['id' => 2, 'desc' => '待售报警（低水位）', 'warn' => true],
        ['id' => 3, 'desc' => '停水（低压）', 'warn' => true],
        ['id' => 4, 'desc' => '排水（高压）', 'warn' => false],
        ['id' => 5, 'desc' => '机箱内温度低', 'warn' => false],
        ['id' => 6, 'desc' => '机箱内温度高', 'warn' => false]
    ],
    //水位状态
    'water_level_state' => [
        ['id' => 1, 'desc' => '低于低水位', 'warn' => true],
        ['id' => 2, 'desc' => '低水位', 'warn' => false],
        ['id' => 3, 'desc' => '中水位', 'warn' => false],
        ['id' => 4, 'desc' => '高水位', 'warn' => false]
    ],
    //外部水压
    'external_water_pressure' => [
        ['id' => 1, 'desc' => '低压', 'warn' => true],
        ['id' => 2, 'desc' => '正常', 'warn' => false],
        ['id' => 3, 'desc' => '高压', 'warn' => false]
    ],
    //开关机
    'switchgear' => [
        ['id' => 1, 'desc' => '关机', 'warn' => true],
        ['id' => 2, 'desc' => '开机', 'warn' => false]
    ],
    //制水状态
    'water_making_state' => [
        ['id' => 1, 'desc' => '机器不在制水', 'warn' => false],
        ['id' => 2, 'desc' => '机器正在制水', 'warn' => false]
    ],
    //售水状态
    'water_selling_state' => [
        ['id' => 1, 'desc' => '机器待售', 'warn' => false],
        ['id' => 2, 'desc' => '机器正在售水', 'warn' => false]
    ],
    //冲洗开关状态
    'flush_switch_state' => [
        ['id' => 1, 'desc' => '冲洗开关关闭', 'warn' => false],
        ['id' => 2, 'desc' => '冲洗开关开启', 'warn' => false]
    ],
    //运行开关状态
    'running_switch_state' => [
        ['id' => 1, 'desc' => '机器停止运行', 'warn' => true],
        ['id' => 2, 'desc' => '机器正在运行', 'warn' => false]
    ],
    //保温开关状态
    'insulation_switch_state' => [
        ['id' => 1, 'desc' => '保温开关关闭', 'warn' => false],
        ['id' => 2, 'desc' => '保温开关开启', 'warn' => false]
    ],
    //内部加热状态
    'internal_heating_state' => [
        ['id' => 1, 'desc' => '内部加热关闭', 'warn' => false],
        ['id' => 2, 'desc' => '内部加热开启', 'warn' => false]
    ],
    //门开关状态
    'gate_switch_state' => [
        ['id' => 1, 'desc' => '门闭合', 'warn' => false],
        ['id' => 2, 'desc' => '门打开', 'warn' => false]
    ],
    //外部加热状态
    'external_heating_state' => [
        ['id' => 1, 'desc' => '外部加热关闭', 'warn' => false],
        ['id' => 2, 'desc' => '外部加热开启', 'warn' => false]
    ],
];
return $config;
