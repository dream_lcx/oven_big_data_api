<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/1/29 0029
 * Time: 上午 11:02
 */

namespace app\Pack;

use Server\CoreBase\SwooleException;
use Server\Pack\IPack;


class IotPack implements IPack
{
    protected $package_length_type = 'n';//数据长度字节数
    protected $package_length_type_length = 2;
    protected $package_length_offset = 11;
    protected $package_body_offset = 15;

    protected $last_data = null;
    protected $last_data_result = null;

    /**
     * 数据包编码
     * @param $buffer
     * @return string
     * @throws SwooleException
     */
    public function encode($buffer)
    {
        //|版本|设备|指令|数据|
        //| 0~1|2 ~ 8|9|10~10+n|
        //获取长度
        $length = strlen($buffer) - 10;
        //|起始|版本|设备ICCID|指令|长度|数据|校验
        //| 0 |1~2 | 3  ~  9 |10 |11 ~ 12 |13~13+n|13+n+1~13+n+3|
        $version = substr($buffer, 0, 2);
        $device_sn = substr($buffer, 2, 7);
        $command = substr($buffer, 9, 1);
        $data = substr($buffer, 10, $length);
        $lengthStr = pack($this->package_length_type, $length);
        $newBuffer = $version . $device_sn . $command . $lengthStr . $data;
        //制作校验码
        $crcStr = hex2bin("f0") . $newBuffer;
        $crc = $this->crc16($crcStr);
        $check = hex2bin($crc);

        if (bin2hex($command) != 'b1') {
            $this->p($this->msgSpace(bin2hex(hex2bin('f0') . $newBuffer . $check)), '发包数据');
        }
        return hex2bin('f0') . $newBuffer . $check;
    }

    /**
     * @param $buffer
     * @return string
     */
    public function decode($buffer)
    {

        //|起始|版本|设备ICCID|指令|长度|数据|校验
        //| 0 |1~2 | 3  ~  9 |10 |11 ~ 12 |13~13+n|13+n+1~13+n+3|
        //去掉前1个字节（起始）
        $ret = substr($buffer, $this->package_length_type_length - 1);
        var_dump($ret);
        //获取后俩个校验字节
        $crc = substr($ret, -2);
        //这里进行校验失败抛异常
        //去掉2个字节
        $str = substr($buffer, 0, -2);
        if ($this->crc16($str) != bin2hex($crc)) {
            //echo "数据被篡改-新主板";
            throw new SwooleException('数据被篡改');
            //写日志,非法数据,数据被篡改(需要写日志,排查系统是否有非法连接)
            //数据异常
        }
        //去除校验字节
        $ret = substr($ret, 0, strlen($ret) - 2);
        return $ret;
    }

    public function pack($data, $topic = null)
    {
        try {
            return $this->encode(hex2bin($data));
        } catch (\Throwable $e) {
            var_dump("pack数据错误");
        }
    }

    public function unPack($data)
    {
        var_dump($data);
        $r_str = bin2hex($data);

        $data = $this->decode($data);
        //|版本  | 设备 |指令|数据长度|数据|
        //| 0~1 |2 ~ 8|   9|10~22|12~12+n|
        $version = substr($data, 0, 2);
        $device_sn = substr($data, 2, 7);
        $command = $data[9];
        $receivedData = substr($data, 12);
        $data = [];
        $data['version'] = bin2hex($version);
        $data['device_sn'] = bin2hex($device_sn);
        $data['command'] = bin2hex($command);
        $data['data'] = bin2hex($receivedData);
        if ($data['command'] != 'b1') {
            $this->p($this->msgSpace($r_str), '收包数据');
        }
        return $data;
    }

    /**
     * 把收包/发包数据用空格分开
     */
    public function msgSpace($msg)
    {
        $data = hex2bin($msg);
        $str = '';
        try {
            $start = bin2hex($data[0]);
            $crc2 = bin2hex(substr($data, -2));

            $version = bin2hex(substr($data, 1, 2));
            $device_sn = bin2hex(substr($data, 3, 7));
            $command = bin2hex($data[10]);
            $len = bin2hex(substr($data, 11, 2));
            $receivedData = bin2hex(substr($data, 13, -2));

            $str = $start . ' ' . $version . ' ' . $device_sn . ' ' . $command . ' ' . $len . ' ' . $receivedData . ' ' . $crc2;
        } catch (\Throwable $th) {

        }

        if (str_replace(' ', '', $str) != $msg) {
            $str = $msg;
        }
        return $str;
    }

    public function getProbufSet()
    {
        return [
            'open_length_check' => true,
            'package_length_type' => $this->package_length_type,
            'package_length_offset' => $this->package_length_offset,       //第N个字节是包长度的值 一个字节包含8个二进制位 一个十六进制可表示4个二进制位 一个字节可以由2个十六进制表示
            'package_body_offset' => $this->package_body_offset,       //第几个字节开始计算长度
            'package_max_length' => 2000000,  //协议最大长度)
        ];
    }

    public function errorHandle(\Throwable $e, $fd)
    {
        get_instance()->close($fd);
    }

    //校验
    protected function crc16($string)
    {
        $crc = 0xFFFF;
        for ($x = 0; $x < strlen($string); $x++) {
            $crc = $crc ^ ord($string[$x]);
            for ($y = 0; $y < 8; $y++) {
                if (($crc & 0x0001) > 0) {
                    $crc = ($crc >> 1);
                    $crc = $crc ^ 0x8088;
                } else {
                    $crc = $crc >> 1;
                }
            }
        }
        $crcRet = sprintf('%02x%02x', floor($crc / 256), $crc % 256);
        return $crcRet;
    }

    //写数据包日志
    protected function p($val, $title = null, $starttime = '')
    {
        print_r('[' . date("Y-m-d H:i:s") . ']:');
        if ($title != null) {
            print_r('[' . $title . ']:');
        }
        print_r($val);
        print_r("\r\n");
    }
}
