<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/1/29 0029
 * Time: 上午 11:02
 */

namespace app\Pack;

use Server\CoreBase\SwooleException;
use Server\Pack\IPack;


class WaterHousePack implements IPack
{
    protected $package_length_type = 'n';
    protected $package_length_type_length = 2;
    protected $package_length_offset = 11;
    protected $package_body_offset = 15;

    protected $last_data = null;
    protected $last_data_result = null;

    /**
     * 数据包编码
     * @param $buffer
     * @return string
     * @throws SwooleException
     */
    public function encode($buffer)
    {
        //|设备|指令|数据|
        //| 0 ~ 9 |10|11 ~ 11+n|
        //|设备|指令|
        $prefixStr = substr($buffer,  0,$this->package_length_offset);
        //|数据|
        $dataStr = substr($buffer,  $this->package_length_offset);
        //获取长度
        $length = strlen($buffer) - 11;
        //制作校验码
        $crcStr = $prefixStr.pack($this->package_length_type, $length).$dataStr;
        $crc = $this->crc16($crcStr);
        $check = hex2bin($crc);
        //|设备|命令字|数据长度|数据|校验
        //| 0 ~ 9 | 10 | 11 ~ 12 |12~12+n|12+n+1~12+n+3|
        return $crcStr . $check;
    }

    /**
     * @param $buffer
     * @return string
     */
    public function decode($buffer)
    {
        //|设备|命令字|数据长度|数据|校验
        //| 0 ~ 9 | 10 | 11 ~ 12 |13~13+n|13+n+1~13+n+3|
        //获取后俩个校验字节
        $crc = substr($buffer,  -2);
        //这里进行校验失败抛异常
        //去掉2个字节
        $str = substr($buffer,  0,-2);
        if($this->crc16($str) != bin2hex($crc)){
            //写日志,非法数据,数据被篡改
            echo "数据被篡改"."\n";
            //throw new SwooleException('data exception');
        }
        //获取数据部分
        $data = substr($str, $this->package_length_offset+$this->package_length_type_length);
        $ret = substr($buffer, 0, $this->package_length_offset);

        return $ret.$data;
    }

    public function pack($data, $topic = null)
    {        
        return $this->encode(hex2bin($data));
    }

    public function unPack($data)
    {
        $data = $this->decode($data);
        //|设备|指令|数据|
        //| 0 ~ 9 |10|11~10+n|
        $device_sn = substr($data,0,10);
        $command = $data[10];
        $receivedData = substr($data, 11);
        $data = [];
        $data['device_sn'] = bin2hex($device_sn);
        $data['command'] = bin2hex($command);
        $data['data'] = bin2hex($receivedData);
        var_dump($data);
        return $data;
    }

    public function getProbufSet()
    {
        return [
            'open_length_check' => true,
            'package_length_type' => $this->package_length_type,
            'package_length_offset' => $this->package_length_offset,       //第N个字节是包长度的值
            'package_body_offset' => $this->package_body_offset,       //第几个字节开始计算长度
            'package_max_length' => 2000000,  //协议最大长度)
        ];
    }

    public function errorHandle(\Throwable $e, $fd)
    {
        get_instance()->close($fd);
    }

    //校验
    protected function crc16($string)
    {
        $crc = 0xFFFF;
        for ($x = 0; $x < strlen ($string); $x++) {
            $crc = $crc ^ ord($string[$x]);
            for ($y = 0; $y < 8; $y++) {
                if (($crc & 0x0001) >0) {
                    $crc = ($crc >> 1) ;
                    $crc = $crc ^ 0xa001;
                } else {
                    $crc = $crc >> 1;
                }
            }
        }
        $crcRet = sprintf('%02x%02x', floor($crc/256),$crc%256 );
        return $crcRet;
    }
}
