<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/1/29 0029
 * Time: 上午 11:02
 */

namespace app\Pack;

use Server\CoreBase\SwooleException;
use Server\Pack\IPack;


class WsPack implements IPack
{
    protected $package_length_type = 'c';
    protected $package_length_type_length = 1;
    protected $package_length_offset = 1;
    protected $package_body_offset = 13;

    protected $last_data = null;
    protected $last_data_result = null;

    /**
     * 数据包编码
     * @param $buffer
     * @return string
     * @throws SwooleException
     */
    public function encode($buffer)
    {
        //|版本|设备|指令|数据|
        //| 0 |1 ~ 7|8|9~9+n|
        //获取长度
        $length = strlen($buffer) - 9;
        //制作校验码
        $crcStr = hex2bin("f0") . pack($this->package_length_type, $length).$buffer;
        $crc = $this->crc16($crcStr);
        $check = hex2bin($crc);
        //|起始|长度|版本|设备|指令|数据|校验 f0 00 01 f56a0137bb8ede b1 d45e
        //| 0 | 1 | 2 |3 ~ 9|10|11~11+n|11+n+1~11+n+3|
        if(1){
            var_dump('下发数据:'.bin2hex(hex2bin('f0') . pack($this->package_length_type, $length).$buffer . $check).'__:'.time());
        }
        return hex2bin('f0') . pack($this->package_length_type, $length).$buffer . $check;
    }

    /**
     * @param $buffer
     * @return string
     */
    public function decode($buffer)
    {
        //|起始|长度|版本|设备|指令|数据|校验
        //| 0 | 1 | 2 |3 ~ 9|10|11~11+n|11+n+1~11+n+3|
        //去掉前2个字节（起始和长度）
        $ret = substr($buffer, $this->package_length_type_length + 1);
        //获取后俩个校验字节
        $crc = substr($ret,  -2);
        //这里进行校验失败抛异常
        //去掉2个字节
        $str = substr($buffer,  0,-2);
        if($this->crc16($str) != bin2hex($crc)){
            //写日志,非法数据,数据被篡改
            echo "数据被篡改"."\n";
            //throw new SwooleException('data exception');
        }
        //去除校验字节
        $ret = substr($ret, 0, strlen($ret) - 2);
        return $ret;
    }

    public function pack($data, $topic = null)
    {
        var_dump($data);
        return $data;
    }

    public function unPack($data)
    {
        var_dump('上传数据:'.$data);

        $info = [];
        $info['data'] = $data;
        $info['controller_name'] = 'TestController';
        $info['method_name'] = 'onMessage';
        return $info;
    }

    public function getProbufSet()
    {
        return [
            'open_length_check' => true,
            'package_length_type' => $this->package_length_type,
            'package_length_offset' => $this->package_length_offset,       //第N个字节是包长度的值 一个字节包含8个二进制位 一个十六进制可表示4个二进制位 一个字节可以由2个十六进制表示
            'package_body_offset' => $this->package_body_offset,       //第几个字节开始计算长度
            'package_max_length' => 2000000,  //协议最大长度)
        ];
    }

    public function errorHandle(\Throwable $e, $fd)
    {
        get_instance()->close($fd);
    }

    //校验
    protected function crc16($string)
    {
        $crc = 0xFFFF;
        for ($x = 0; $x < strlen ($string); $x++) {
            $crc = $crc ^ ord($string[$x]);
            for ($y = 0; $y < 8; $y++) {
                if (($crc & 0x0001) >0) {
                    $crc = ($crc >> 1) ;
                    $crc = $crc ^ 0x8408;
                } else {
                    $crc = $crc >> 1;
                }
            }
        }
        $crcRet = sprintf('%02x%02x', floor($crc/256),$crc%256 );
        return $crcRet;
    }
}
