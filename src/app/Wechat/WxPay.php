<?php

namespace app\Wechat;



class WxPay
{

    var $parameters;

    var $response;

    var $result;

    var $url;

    var $curl_timeout;

    var $appid;

    var $secret;

    var $key;

    var $mchid;
    var $type;
    var $path = __DIR__ . '/cert/'; //证书和公匙的牡蛎
    var $RSA_PRIVATE;   //私钥
    var $RSA_PUBLIC = __DIR__ . '/cert/public_key_pkcs8.pem';   //公钥  //需要PKCS#8格式  openssl rsa -RSAPublicKey_in -in  <filename> -out <out_put_filename>
    const UNIFIED_ORDER = 'https://api.mch.weixin.qq.com/pay/unifiedorder';//统一下单
    const TRANSFERS = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers'; //企业付款到零钱
    const GET_TRANSFERS_INFO = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo';//查询企业付款到零钱
    const PAY_BANK = 'https://api.mch.weixin.qq.com/mmpaysptrans/pay_bank';//企业付款到银行卡
    const GET_APY_BANK = 'https://api.mch.weixin.qq.com/mmpaysptrans/query_bank';//查询企业付款到银行卡
    const GET_PUBLIC_KEY = 'https://fraud.mch.weixin.qq.com/risk/getpublickey';//获取RSA加密公钥API
    var $apiclient_key_pem;
    var $apiclient_cert_pem;
    var $ip;

    function __construct($config)
    {
        $this->appid = $config['appId'];
        $this->secret = $config['appSecret'];
        $this->key = $config['pay_key'];
        $this->mchid = $config['mchid'];
        $this->ip = $config['ip']??'0.0.0.0';
        $this->url = $config['url'] ?? self::UNIFIED_ORDER;
        $this->curl_timeout = 30;
        $this->type = $config['type'] ?? 1;//1 支付 2企业付款到零钱
        $this->apiclient_key_pem = __DIR__ . '/cert/' . $this->mchid . '/apiclient_key.pem';
        $this->apiclient_cert_pem = __DIR__ . '/cert/' . $this->mchid . '/apiclient_cert.pem';

    }

    function trimString($value)
    {
        $ret = null;
        if (null != $value) {
            $ret = $value;
            if (strlen($ret) == 0) {
                $ret = null;
            }
        }
        return trim($ret);
    }

    public function createNoncestr($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }

            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    public function getSign($Obj)
    {
        foreach ($Obj as $k => $v) {
            $Parameters[$k] = $v;
        }
        $String = $this->formatBizQueryParaMap($Parameters, false);

        $String = $String . "&key=" . $this->key;

        $String = md5($String);

        $result_ = strtoupper($String);

        return $result_;
    }

    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        return $xml;
    }

    public function xmlToArray($xml)
    {
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $array_data;
    }

    /**
     * 以post方式提交xml到对应的接口url
     *
     * @param string $xml 需要post的xml数据
     * @param string $url url
     * @throws WxPayException
     */
    public function postXmlCurl($xml, $url)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->curl_timeout);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new WxPayException("curl出错，错误码:$error");
        }
    }

    /**
     *    作用：使用证书，以post方式提交xml到对应的接口url
     */
    function postXmlSSLCurl($xml, $url, $second = 300)
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //设置证书
        //使用证书：cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLCERT, $this->apiclient_cert_pem);


        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLKEY, $this->apiclient_key_pem);

        //post提交方式
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            echo "curl出错，错误码:$error" . "<br>";
            echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }

    function setParameter($parameterData, $parameterArray = ['openid', 'body', 'out_trade_no', 'total_fee', 'notify_url', 'trade_type'])
    {
        if (empty($parameterData)) {
            throw new \Server\CoreBase\SwooleException('请传入您要支付的信息');
        }

        foreach ($parameterArray as $val) {
            if ($parameterData[$val] == false) {
                throw new \Server\CoreBase\SwooleException('支付参数' . $val . ' 不能为空');
            }
            $this->parameters[$this->trimString($val)] = $this->trimString($parameterData[$val]);
        }
        return $this->parameters; // 返回支付参数
    }
    //服务商支付设置参数
    function setServiceParameter($parameterData)
    {
        // 需要传入的支付信息
        $parameterArray = array(
            'body',
            'out_trade_no',
            'total_fee',
            'notify_url',
            'trade_type',
            'sub_mch_id',
            'sub_openid',
            'sub_appid'
        );

        if (empty($parameterData)) {
            throw new \Server\CoreBase\SwooleException('请传入您要支付的信息');
        }

        foreach ($parameterArray as $val) {
            if ($parameterData[$val] == false) {
                throw new \Server\CoreBase\SwooleException('支付参数' . $val . ' 不能为空');
            }
            $this->parameters[$this->trimString($val)] = $this->trimString($parameterData[$val]);
        }

        return $this->parameters; // 返回支付参数
    }

    function createXml()
    {
        if ($this->type == 2) {
            $this->parameters["mch_appid"] = $this->appid; //公众账号ID
            $this->parameters["mchid"] = $this->mchid; //商户号
            $this->parameters["nonce_str"] = $this->createNoncestr(); //随机字符串
            $this->parameters["spbill_create_ip"] = $this->ip; //终端ip
            $this->parameters["sign"] = $this->getSign($this->parameters); //签名
        } else {
            $this->parameters["appid"] = $this->appid;
            $this->parameters["mch_id"] = $this->mchid;
            $this->parameters["nonce_str"] = $this->createNoncestr();
            $this->parameters["spbill_create_ip"] = $this->ip; //终端ip
            $this->parameters["sign"] = $this->getSign($this->parameters);

        }

        return $this->arrayToXml($this->parameters);
    }

    function getPrepayId()
    {
        $this->postXml();
        $this->result = $this->xmlToArray($this->response);
        if(empty($this->result['prepay_id']??'')){
            var_dump($this->result);
        }
        $prepay_id = $this->result["prepay_id"];
        return $prepay_id;
    }

    function postXml()
    {
        $xml = $this->createXml();
        $this->response = $this->postXmlCurl($xml, $this->url, $this->curl_timeout);
        return $this->response;
    }

    function postXmlSSL()
    {
        $xml = $this->createXml();
        $this->response = $this->postXmlSSLCurl($xml, $this->url, $this->curl_timeout);
        return $this->response;
    }

    function getResult()
    {
        $this->postXml();
        $this->result = $this->xmlToArray($this->response);
        return $this->result;
    }

    /**
     * RSA数据加密解密
     * @param string $data
     * @param string $type encode加密  decode解密
     * @return string
     * @throws \Exception
     * @date 2019/4/11 10:12
     * @author ligang
     */
    public function RAS_openssl(string $data, string $type = 'encode')
    {
        if (empty($data)) {
            return 'data参数不能为空';
        }

        //私钥解密
        if ($type == 'decode') {
            $private_key = openssl_pkey_get_private($this->RSA_PRIVATE);
            if (!$private_key) {
                return ('私钥不可用');
            }
            $return_de = openssl_private_decrypt(base64_decode($data), $decrypted, $private_key);
            if (!$return_de) {
                return ('解密失败,请检查RSA秘钥');
            }
            return $decrypted;
        }
        //检查是否存在公钥
        if (!file_exists($this->RSA_PUBLIC)) {
//            throw new \Exception('公匙不存在');
            $this->get_public_key();
        }
        //公钥加密
        $RSA_PUBLIC = file_get_contents($this->RSA_PUBLIC);
        $key = openssl_pkey_get_public($RSA_PUBLIC);
        if (!$key) {
            return ('公钥不可用');
        }
        $return_en = openssl_public_encrypt($data, $crypted, $key, OPENSSL_PKCS1_OAEP_PADDING);
        if (!$return_en) {
            return ('加密失败,请检查RSA秘钥');
        }
        return base64_encode($crypted);
    }

    /**
     * 获取公匙
     * @desc 描述
     * @throws \Exception
     * @date 2019/4/11 10:12
     * @author ligang
     */
    public function get_public_key()
    {
        if (!$this->checkCertificate()) {
            throw new \Exception('证书不存在');
        }
        $pub_param = [
            'nonce_str' => $this->createNoncestr(),
            'sign_type' => 'MD5',
            'mch_id' => $this->mchid,
        ];
        $pub_param["sign"] = $this->getSign($pub_param);
        $xml = $this->arrayToXml($pub_param);
        $result = $this->postXmlSSLCurl($xml, self::GET_PUBLIC_KEY);
        $data = $this->xmlToArray($result);
        if ($data['return_code'] == 'FAIL') {
            throw new \Exception($data['return_msg']);
        }
        if ($data['result_code'] == 'FAIL') {
            throw new \Exception($data['err_code_des']);
        }

        //公匙落地为文件
        file_put_contents($this->path . 'public_key_pkcs1.pem', $data['pub_key']);
        //转换公匙格式为PKCS#8
        $commad = 'openssl rsa -RSAPublicKey_in -in ' . $this->path . 'public_key_pkcs1.pem -out ' . $this->path . 'public_key_pkcs8.pem';
        exec($commad);
        $this->RSA_PUBLIC = $this->path . 'public_key_pkcs8.pem';
    }

    /**
     * 检查证书
     * @desc 描述
     * @author ligang
     * @date 2019/4/11 9:42
     */
    public function checkCertificate()
    {
        if (!file_exists($this->apiclient_cert_pem) || !file_exists($this->apiclient_key_pem)) {
            return false;
        }
        return true;
    }
}
