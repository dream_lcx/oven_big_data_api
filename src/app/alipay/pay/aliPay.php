<?php

namespace app\alipay\pay;

use app\alipay\pay\aop\AopClient;
use app\alipay\pay\aop\request\AlipaySystemOauthTokenRequest;
use app\alipay\pay\aop\request\AlipayTradeCreateRequest;

/*
 * 支付宝支付
 */
class aliPay
{

    //商户私钥
    private $rsaPrivateKey = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBoxpYSAc2yjHbHkQGyiQGLB6szd9rrqfjglxhDgNTtAmGQhOT1LeENPCQsEkJRQyHSIssQPg19wIqe+T1ayh+XkwYaOBO2d1wfp57oiK7nzzypMphUcKtiq/AAOIEZQJTTerBPDZFuGJbR0y3WGUZb+UI7qaQaGQJpTwHj/NnmUlSDkQ6l0jY3eEhqOjIiGgiZWtDR40GuuhCkWxtDoA/BDrlFW2JN16qGgudnBdw5i7vomCLfRIDzbK4sreYgOStoviwk5bUMsjUgIIdNw+g7P4Kqc4kznE10h1/2qT8u0wL+0nbQ3bYBt3BfFbJtYLS1l1zbRE52PtzKcclN6+fAgMBAAECggEAV+lepT17PumRM7Hrmdsc4N6skq0t9N1FVyCBOwQAt5Og3d4TXq3jIZGxDb2uWc3seBV247TPXfE7E8pe8xsPI9sYqXj9nBtccrw7B0ZlXWzYLQgZgjOq3HrvQjqXCznggNOW/iGyAEfQ02ufl7onHT/nRs5oQAe6x1u5QLua6cZvfvAPydGlXJWBo1irc9A0oNygdFde4tS4Qe7e2AJ2JCY07f18+CuFza0yI9IbaHkZfrt67FUF5z26eBzEea3uRguoMsZhMaiduufnNYAifGdn9LyUHKsay1/TFC3T6dwfIqRwdehCXSAw2dUePiGyyTWnm6eD8fiVgBivxlBzyQKBgQC3mOZ+Xpoie9WA7+OruZIewUIBCzqsHLXByoiNgbbVJm/d31i3QVFtW4aG9bQ2Z6nJRBKoZKdc/aHvvu5N83jDYHQe8AHSjvZbM8xnJ9hTXreq6TMZVqeXMXDQUKp1tcLdEJ63vLWeEfIxqrG/Y7K68zWdlzJsKBjIDVe74I+VSwKBgQC0wqUayE8/rxHvt1GRwOJ7Bq9FO6PRNHeQecGW+aqRbkz3JA5ummGaOqJtNjogoA1OzCQjaE+nmfHlfdpMScVRZLgzaydtlFSMtmSLgSOXAR/jlXiFh64txfKWjUOed8mvEjKLOml2wBwpHR87LStvxxwrFCAO3qG/7NOg+bcefQKBgQC01zWKGdLGzqUxeJkP6BymkINcFdx39VuOg9lXUQJvF4ouyDVRZFQ5XGK5AsG8qQUTSyr5lRNJREbCqJWAUKaOA669XnjdFpZQpUfb/+JT1EMjZMpBVhqDkV5mwl30zdEgAKPlpUpbZZ/jDhhAxYoiHTedf3VaFXC7dHELjRjA2wKBgAKDPiZMKIXbGiW556FeDAfnxOHyfUl4MD1K4/0ERXG2Fc3Nq4GwFbE+JWndOhuf/e9deRcg6RKMWkd1Y2ZyFBMYklLsp2SnjjFSi71As0zzEA1q030PsMUkHkYneowmYCBV4Wv8COFEtF+B7yA0dYkdPHAA3J04TQlM8ngnYHtFAoGAcO1dkjzbmvZ8SG9u/vjkymgb4X2/x13u936eeXsgl4fTtvznxm7LKrCvsySQdXKr+Df9S8rkhk++Tt/h3Mw6ZAkXTZa0MuhkjaL8ljf2QJF9icI09BSsX9PvOPS9kDWgG90NuZ4owQ7KMawmHz0PoEnWikhlL6KoUDmQHI2tk68=';

    //商户公钥
    private $rsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgaMaWEgHNsox2x5EBsokBiwerM3fa66n44JcYQ4DU7QJhkITk9S3hDTwkLBJCUUMh0iLLED4NfcCKnvk9Wsofl5MGGjgTtndcH6ee6Iiu5888qTKYVHCrYqvwADiBGUCU03qwTw2RbhiW0dMt1hlGW/lCO6mkGhkCaU8B4/zZ5lJUg5EOpdI2N3hIajoyIhoImVrQ0eNBrroQpFsbQ6APwQ65RVtiTdeqhoLnZwXcOYu76Jgi30SA82yuLK3mIDkraL4sJOW1DLI1ICCHTcPoOz+CqnOJM5xNdIdf9qk/LtMC/tJ20N22AbdwXxWybWC0tZdc20ROdj7cynHJTevnwIDAQAB';

    //应用公钥
    // private $a = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgaMaWEgHNsox2x5EBsokBiwerM3fa66n44JcYQ4DU7QJhkITk9S3hDTwkLBJCUUMh0iLLED4NfcCKnvk9Wsofl5MGGjgTtndcH6ee6Iiu5888qTKYVHCrYqvwADiBGUCU03qwTw2RbhiW0dMt1hlGW/lCO6mkGhkCaU8B4/zZ5lJUg5EOpdI2N3hIajoyIhoImVrQ0eNBrroQpFsbQ6APwQ65RVtiTdeqhoLnZwXcOYu76Jgi30SA82yuLK3mIDkraL4sJOW1DLI1ICCHTcPoOz+CqnOJM5xNdIdf9qk/LtMC/tJ20N22AbdwXxWybWC0tZdc20ROdj7cynHJTevnwIDAQAB';

    //支付宝公钥
    private $alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyTiRa6bNwBjsainVhQRjhx/VW4Dr9tjHbMKaVBytCmmEtIEhbeLq9ix02WJoYyD4BOPtUGI1Jlq/eA4kscC5dSJSuL7pJytNfnSgkUq5MpboF/f6aC7AYZtEmb9OooaLA9ZXav/RqMTGt1Q5KbY4YZDZs/d28cb516Rl8x6XOMNklDYDWhzOA0oMadXN74lp5OMNo7z/ISbEMGAOczz2BIxHnbYUSA5rPuGVFwApNERc5MeAVYqojXjJpDztp24LLGjvjC3WJmPpDrwJMvcA2Z92V9uGHNPzAF+KoKzxHRzRaFhVwRohBFSmP1EupIud4RLxbzRenQGWzfOGgOQHEwIDAQAB';

    //支付宝APPID
    private $appId = '2019110468881302';
    
    private $aop;

    function __construct($config){

        //1、execute 使用
        $this->aop = new AopClient ();

        $this->aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $this->aop->appId = $config['appId']; //$this->appId; // '2018111962265438';
        $this->aop->rsaPrivateKey = $config['rsaPrivateKey']; //$this->rsaPrivateKey;
        $this->aop->alipayrsaPublicKey = $config['alipayrsaPublicKey']; //$this->alipayrsaPublicKey;
        $this->aop->apiVersion = '1.0';
        $this->aop->signType = 'RSA2';
        $this->aop->postCharset='utf-8';
        $this->aop->format='json';

        if(isset($config['encryptKey']))
            $this->aop->encryptKey = $config['encryptKey'];
    }

    /**
     * 统一收单交易创建接口
     */
    public function TradeCreate($param)
    {
        try {
            $request = new AlipayTradeCreateRequest();
            $content['out_trade_no'] = $param['out_trade_no'];
            $content['total_amount'] = sprintf('%.2f', $param['total_amount']);
            $content['subject'] = $param['subject'];
            $content['buyer_id'] = $param['buyer_id'];//'2088802486734682';
            $content=json_encode($content);
            $request->setNotifyUrl($param['notify_url']);
            $request->setBizContent($content);
            $result = $this->aop->execute ( $request);



            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if(!empty($resultCode)&&$resultCode == 10000){
                $data = ['code'=>1000, 'msg'=>'', 'data'=>$result->$responseNode->trade_no];
            }
            else{
                var_dump($result);
                $data = ['code'=>-1001, 'msg'=>$result->$responseNode->msg, 'data'=>''];
            }

        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            $data = ['code'=>-1000, 'msg'=>$th->getMessage(), 'data'=>''];
        }

        // var_dump($data);
        return $data;
    }

    
    /**
     * 支付回调验证
     */
    public function checkSign($arr)
    {
        try {
            // $string = file_get_contents("php://input"); //接收POST数据
            // $string = $this->http_input->getRawContent();
	    
            // parse_str($string,$arr);	
            // $arr['sign'] = str_replace(" ","+",$arr["sign"] ?? []);
            
            //验证签名
            $flag = $this->aop->rsaCheckV1($arr , NULL, "RSA2");


            if(!empty($arr) && $flag) {
                //以下为回调业务逻辑代码
                if ($arr['trade_status'] == 'TRADE_SUCCESS') {
                    //支付成功业务处理

                    $data = ['code'=>1000, 'msg'=>'支付成功', 'data'=>$arr];
                }
                else{
                    $data = ['code'=>-1001, 'msg'=>'支付失败', 'data'=>$arr];
                }
            }
            else{
                $data = ['code'=>-1002, 'msg'=>'验签失败', 'data'=>$arr];
            }

        } catch (\Throwable $th) {
            $data = ['code'=>-1000, 'msg'=>$th->getMessage(), 'data'=>$arr];
        }

        // var_dump($data);
        return $data;
    }


    
    /**
     * 服务端获取 access_token  
     */
    public function get_access_token($auth_code)
    {
        try {
            $request = new AlipaySystemOauthTokenRequest();
            
            $request->setGrantType('authorization_code');
            $request->setCode($auth_code);
            // $request->setRefreshToken('201208134b203fe6c11548bcabd8da5bb087a83b');

            $result = $this->aop->execute( $request);
            // var_dump($result);

            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code ?? '';
            $resultAccessToken = $result->$responseNode->access_token ?? '';
            if(!empty($resultCode)&&$resultCode == 10000 || !empty($resultAccessToken)){
                //alipay_user_id
                $arr = [
                    'user_id'       =>  $result->$responseNode->user_id,
                    'access_token'  =>  $result->$responseNode->access_token,
                    'expires_in'  =>  $result->$responseNode->expires_in,
                ];
                $data = ['code'=>1000, 'msg'=>'成功', 'data'=>$arr];
            }
            else{
                $msg = $result->error_response->sub_msg ?? '';
                if(empty($msg)) $msg = $result->error_response->msg ?? '';

                $data = ['code'=>-1001, 'msg'=>$msg, 'data'=>[]];
            }

        } catch (\Throwable $th) {
            $data = ['code'=>-1000, 'msg'=>$th->getMessage(), 'data'=>[]];
        }

        // var_dump($data);
        return $data;
    }

    
    /**
     * 支付解密
     */
    public function decrypt($encryptedData, $sign)
    {
        try {
            //验证签名
            // $arr = [
            //     'response'   =>  '"'.$encryptedData.'"',
            //     'sign'          =>  $sign,
            //     'sign_type'          =>  'RSA2',
            //     'charset'          =>  'UTF-8',
            // ];
            // $flag = $this->aop->rsaCheckV1($arr, NULL, "RSA2");
            // $data = ['code'=>-1002, 'msg'=>'验签失败a'.$flag, 'data'=>$flag];
            // return $data;
            
            $key = $this->aop->encryptKey;
            $aesKey=base64_decode($key);
            $iv = 0;

            $aesIV=base64_decode($iv);

            $aesCipher=base64_decode($encryptedData);

            $result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
            $result = json_decode($result, true) ?? [];
            if($result && isset($result['code']) && $result['code'] == '10000') {
                $data = ['code'=>1000, 'msg'=>'解密成功', 'data'=>$result];
            }
            else{
                $data = ['code'=>-1002, 'msg'=>'解密失败', 'data'=>$result];
            }

        } catch (\Throwable $th) {
            $data = ['code'=>-1000, 'msg'=>$th->getMessage(), 'data'=>[]];
        }

        // var_dump($data);
        return $data;
    }

}
