<?php

$aliPayconfig = array(
    //应用ID,您的APPID。
    'app_id' => "2016091500519721",
    //商户私钥
    'merchant_private_key' => "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFPdUbCaWY4XU0l9MeAYAO6ffEN/BxPglsL7Vlj+KhJYiFKn12VhY7lEbGl0KL9sX/vIiMLjz8FVU9NbFnAEUbtG88ztCcbfSNJGmdRxFHuj2CV2L4eGc5LgEmnxWWnJaFQaXYOESFlqAgBJIoB0GBM7VJ4UY+BxrtwLERghkgKzlgKfwomPTWQhOnZz5DSJdHG1fJQ1EId909iLINKy1PVi2ZdB70Wu8xk05v+oltqXUK40zWjT0xetjvmTm0ghX5IwqV+7lPotEPBfTZbu0XazXB/x+HUWOJ472n/45ec5w1jsDnYDDUsM1q990u3rL9M+Nd+KCG6/IJE4njsDYnAgMBAAECggEAB/tEkF+Hj/Qvpf0u0dwFrpTRLeOFm0+DWhlkIcP8y8CVEa5yfUNmiuqpIogkbT2DLaIXdHS6iwjh9iQiFaTxJvnaNn+3+VyJtNxwWajyjhB/RLL8kkltA+dUrJdrbDjR7nqFl4ZPX4iE2kbVE0GX6P6Kq36uNPxcnppZj/fPxHlpDCPshAjcYximu7K4jyJwuoih4Z9sa4hv8/u98aBAWbzPMgC1nRdTUvN69gjxRif0XvPuEgoWXNDwAF3/HHKZT+zUMzqkoVHiqYn9djEIYG7r+WIM2xG/fgR3wGM9Uakw1dynKYv5uQOLPOqwi95ZJMnugyGjhRhk3UZb6TZDgQKBgQD010hojinClQSxphMJCqTby1/cqeSHgm7A6vtZZLkJScwxYal8i3TfND07Xpb9IH6XW+F4BTuLp0GRBYF6Ob6Po2ssrqAGa6pAl6L0XCvPbxC8dswS/kh6Q0H6BJz2jKDApqTrjGiDKZkfObzsCc5RXa0m2gBT691yyEGdO5JU1wKBgQDOOy0xiCdWKyFnw2ilyk4GrvSWUdOV4jyDw1RmbSgzRku6sQsnctjk6PyyAS5UZohzOA1Q9Zl5RY+xlhxuizO3g9is3EW0DW3ymOqgto3Go2bKWa0766GRpgIf1tZD8aExXoEYVBm6UJ4G9M8SfZfQI0ijUJnUQd7QkG+8C+mvMQKBgE1GFOhK1gs/lPesfp1/6oAkM+ElDadnFyV0BFg2Xd/8lqGPPTq9mOlJR3oRZH4HX2zN5t9GZ4CwDRWEM1UsHGShIxY7Fu5v1JAGS7vryzfwyctxgv8LmjxcNdp+0jtOLv4emjZtXh7nZhhg1Kt6fM9ZTWjRrQDPlFjBs1O4lXqJAoGAQmdPf/llIRu2H+ENmwRu5zwCSTyDsmksLwj/7hMLtKbXz38eJk+Wgu56Hx6+OjLhkKflscHC7wd04yuAcB+mQVrUs6QhVX5Vfp26PpFJSUJn4JPLcdXHKkyXtoTIbQwqaOkTDDBYMGOw3nCJFF0YOs6BnwRe47dp9SHsuFHlpqECgYEA1mQ7BvZj9lzc/jY7eKTPCnsT2yGjjvymiK64UH5xi76UB6Uh5CwIsRcjghXS7eHQ1VdHUj63Cj4+rCMcGJyl6Mlsd/uDJ9OHKeJAtS67qO7loX0yWrgBA/FTIzs38SpNuLDVIOGI3oWyl5JCtFfvdko/opZzJzgFQLLnEPsFvYA=",
    //异步通知地址
    'notify_url' => "http://water.ycw520.top/Partner/Callback/ali_scan_pay",
    //同步跳转
    'return_url' => "http://water.ycw520.top",
    //编码格式
    'charset' => "UTF-8",
    //签名方式
    'sign_type' => "RSA2",
    //支付宝网关
    'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do",
    //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtVex9AygCEror+HFUebTMXRfKEDQp1XBBcTJrbBNJpxJ6XjCByz0dyIRWr3zM2c5ayoUXbCtqB7PdXr5+VAsNgVVSgp1P+R/cJhtdqI48Hod7atuWb/zV9fViTyknuJfBEiWSZG9uefZkKT1FiU6fipRnSJElkG8FIGpo5X4zSbHOds3rR1NSw2oe30l+TBJy5rAfSMnZZ1CU+/Pf11qT8wmiO/zqWs9XpOdKGeiTnaR/zqRVc0EiWFnQiD1DglS9QkQeO3YYmCD42l5mwDX2geimL+wuD8/pfI2e5UD7A9nvQfXUXxabz7i9KO03Z4drurJhKLOjAjKP8b/ZEGOcQIDAQAB",
);
return $aliPayconfig;