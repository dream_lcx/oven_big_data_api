<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/4/24
 * Time: 17:50
 */

namespace app\Library;


class AsyncFile
{
    /**
     * @var string 日志目录
     */
    static public $log_file = LOG_DIR.'/';

    /**
     * @var int 内容格式，1：与SLog一致
     */
    static public  $content_type = 1;

    /**
     *
     * @desc 描述
     * @author ligang
     * @param string $filename
     * @param int $line
     * @return string
     * @throws \Exception
     * @date 2019/4/25 17:53
     */
    static public function read(string $filename,int $line = 30)
    {
        //该方法不无法返回读取内容，尝试PHP读取文件方法
//        return swoole_async_read(self::$log_file.$filename, $callback, $size, $offset);
        //通过linux命令来执行读取
        $filename = self::checkFilename($filename);
        $file = self::$log_file.$filename;
        if (!file_exists($file)){
            throw new \Exception('文件或目录不存在：'.$file);
        }
        $file = escapeshellarg($file);
        $command = 'tail -'.$line.' '.$file;
        exec($command , $output);
        $output = implode('<br />',$output);
        return $output;

    }

    /**
     * 异步写文件
     * @desc 异步写文件
     * @author ligang
     * @param string $filename      文件名，后缀默认log
     * @param string $content       内容
     * @param int $offset           当offset为-1时表示追加写入到文件的末尾
     * @param null $callback        callback函数返回false才会close文件, 否则fd将会在下一次write时被复用
     * @return mixed
     * @date 2019/4/24 18:23
     */
    static public function write(string $filename, string $content, int $offset = -1, $callback = NULL)
    {
        $filename = self::checkFilename($filename);
        if (!file_exists(self::$log_file.$filename)){
            self::checkPath();
            return swoole_async_writefile(self::$log_file.$filename, $content.PHP_EOL);
        }
        return swoole_async_write(self::$log_file.$filename, $content.PHP_EOL, $offset, $callback);
    }

    /**
     * 检查目录是否存在，不存在则创建
     * @desc 检查目录是否存在，不存在则创建
     * @author ligang
     * @param string $path  文件目录
     * @return bool
     * @date 2019/4/25 9:18
     */
    static protected function checkPath()
    {
        if (!file_exists(self::$log_file)){
            $dir = iconv("UTF-8", "GBK", self::$log_file);
            mkdir ($dir,0777,true);
        }
        return true;
    }

    /**
     * 校验文件名
     * @desc 校验文件名
     * @author ligang
     * @param string $filename
     * @return string
     * @date 2019/4/29 11:20
     */
    static protected function checkFilename(string $filename)
    {
        $pathinfo = pathinfo($filename);
        if (!isset($pathinfo['extension'])){
            return $pathinfo['filename'].$pathinfo['dirname'].'log';
        }
        return $filename;
    }

    /**
     * 删除文件
     * @author ligang
     * @param string $path      文件或目录，目录以/结尾
     * @param string $suffix    文件后缀，空为所有文件，区分大小写，目录有效
     * @param bool $today       是否检查当天文件，true当天文件不会被删除，目录有效
     * @return bool
     * @date 2019/1/29 9:57
     */
    static public function delFile(string $path, string $suffix = '', bool $today = true) {
        if (is_dir($path)) {
            $date = date('Ymd', time());
            //是个目录
            $file_array = scandir($path);
            foreach ($file_array as $key => $value) {
                if ($value == '.' || $value == '..') {
                    continue;
                }
                $file_path = $path . $value;
                if ($today) {
                    $filectime = filectime($file_path);
                    if ($filectime !== false && date('Ymd', $filectime) == $date) {
                        continue;
                    }
                }

                if (!empty($suffix)) {
                    if (stripos($value, $suffix) === false) {
                        continue;
                    }
                    unlink($file_path);
                } else {
                    unlink($file_path);
                }
            }
            return true;
        }
        unlink($path);
        return true;
    }
}