<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/11/29
 * Time: 22:06
 */

namespace app\Library;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use app\Library\Factory as LF;

class SLog
{

    static $instance;
    static $STHinstance;
    const DEBUG_LOG = 'Debug';
    const LOG_SUFFIX = '.log';
    const CARD_LOG = 'card';
    const DEVICE_LOG = 'device';

    public static function logConf()
    {
        return array(
            self::CARD_LOG => array(LOG_PATH.'/water/card'.DS,Logger::INFO),
            self::DEVICE_LOG => array(LOG_PATH.'/water/device'.DS,Logger::INFO),
            self::DEBUG_LOG => array(LOG_PATH.'/debug/'.date('Y-m-d').DS,Logger::INFO),

        );
    }


    public static function getSTH($path,$logType)
    {
        //StreamHandler($path.$index.self::LOG_SUFFIX, $logType)
        if (!isset(self::$STHinstance[$path])) {
            self::$STHinstance[$path] = new StreamHandler($path, $logType);
        }
        return self::$STHinstance[$path];
    }

    /**
     * static pool
     * @return object
     */
    public static function SL($type,$index='default')
    {
        $c = $type.$index;
        if (!isset(self::$instance[$c])) {
            list($path,$logType) = self::logConf()[$type];

            //此处简单处理,日志可以写往不同通道,分发处理,可以写进程监听等等
            //详情见https://github.com/Seldaek/monolog  文档
            $log = new Logger($type);
            //设置日志格式
            $stream_handler = self::getSTH($path.$index.self::LOG_SUFFIX, $logType);
            //$stream_handler->setFormatter(LF::getInstance('\\app\\Library\\CliFormatter'));
            $log->pushHandler($stream_handler);

            self::$instance[$c] = $log;
        }
        return self::$instance[$c];
    }


    /**
     * HTTP日志
     * static pool
     * @return object
     */
    public static function SLT($type=self::DEBUG_LOG,$method=false)
    {
        $method = $method ?? ucfirst(Request::getCtrl()).'-'.Request::getMethod();
        if (!isset(self::$instance[$method])) {
            list($path,$logType) = self::logConf()[self::DEBUG_LOG];

            //此处简单处理,日志可以写往不同通道,分发处理,可以写进程监听等等
            //详情见https://github.com/Seldaek/monolog  文档
            $log = new Logger($type);
            //设置日志格式
            $stream_handler = self::getSTH($path.$method.self::LOG_SUFFIX, $logType);
            $stream_handler->setFormatter(LF::getInstance('\\Library\\CliFormatter'));
            $log->pushHandler($stream_handler);

            self::$instance[$method] = $log;
        }
        return self::$instance[$method];
    }
}