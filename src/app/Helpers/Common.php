<?php

use app\Wechat\wxcrypt\WXBizDataCrypt;

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018-09-05
 * Time: 上午 11:26
 */
function getTime()
{
    return time();
}

/**
 * 正则验证 by gj 2020/2/24
 * 返回bool
 */
function pregMatchInput($input, $type = '')
{
    $input = trim($input);
    if (empty($input) && $type != 'float')
        return false;
    switch ($type) {
        case '':
            break;
        case 'name':
            $match = "/^[\x{4e00}-\x{9fa5}A-Za-z0-9]+$/u";//名称验证,只能是汉字或字母或数字
            break;
        case 'account':
            $match = "/^[a-zA-Z0-9_-ÿ][a-zA-Z0-9_-ÿ]+$/";//账号验证,两位以上的字母,数字,或者下划线
            break;
        case 'password':
            $match = '/^[0-9a-z_$]{6,16}$/i';//密码验证 6-16位的数字字母下划线组成
            break;
        case 'phone':
            $match = '#^1[3-9][\d]{9}$#';//手机号格式验证
            break;
        case 'card':
            $match = '/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/';//身份证号格式验证
            break;
        case 'email':
            $match = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';//邮箱格式验证
            break;
        case 'equipment_number':
            $match = "/^[0-9a-z]{2,36}$/i";//设备编号验证,两位以上的字母,数字
            break;
        case 'float':
            $match = '/^\d+(\.\d+)?$/';//非负浮点数验证 可以验证所有的正整数和正小数 包括0
            break;
    }
    return preg_match($match, $input);
}

/*
     *
     * 解密小程序手机号码
     */
function PhoneDecrypt($encryptedData, $sessionKey, $iv, $wx_config)
{
    if (isset($encryptedData)) {
        $pc = new WXBizDataCrypt($wx_config['appId'], $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        $arr = json_decode($data, true);
        return $arr['phoneNumber'];
    } else {
        return '';
    }
}

/**
 * 处理钱的格式 元转成分 ，分转成元
 * 元转分 formatMoney（$money,1）
 * 分转成元 formatMoney（$money,2)
 * @param int $money 钱
 * @param int $unit 单位1 是元 2是分
 * @param int $point 小数点位数
 */
function formatMoney($money, $unit = 2, $point = 2)
{
    if (empty($money)) {
        return 0;
    }
    if ($unit == 1) {//将元转成分
        $money = intval($money * 100);
    } else {
        $reg = '/^[0-9][0-9]*$/';
        $money = $money / 100; //将分转成元
        if ($point == 0 || preg_match($reg, $money)) {
            $money = (int)$money;
        } else {
            $money = sprintf('%.' . $point . 'f', $money);
        }
    }
    return $money;
}

//二维数组查找某一个值，并返回
function searchArray($array, $key1, $value1, $key2 = "", $value2 = "")
{
    $arr = array();
    foreach ($array as $keyp => $valuep) {
        if (empty($key2) || empty($value2)) {
            if ($valuep[$key1] == $value1) {
                array_push($arr, $valuep);
            }
        } else if (empty($key1) || empty($value1)) {
            if ($valuep[$key2] == $value2) {
                array_push($arr, $valuep);
            }
        } else {
            if ($valuep[$key1] == $value1 && $valuep[$key2] == $value2) {
                array_push($arr, $valuep);
            }
        }
    }
    return $arr;
}

//根据key 给二维数组分组
function array_group_by($arr, $key)
{
    $grouped = [];
    foreach ($arr as $value) {
        $grouped[$value[$key]][] = $value;
    }
// Recursively build a nested grouping if more parameters are supplied
// Each grouped array value is grouped according to the next sequential key
    if (func_num_args() > 2) {
        $args = func_get_args();
        foreach ($grouped as $key => $value) {
            $parms = array_merge([$value], array_slice($args, 2, func_num_args()));
            $grouped[$key] = call_user_func_array('array_group_by', $parms);
        }
    }
    return $grouped;
}

/**
 * 函数返回结果
 * @param $code
 * @param string $message
 * @param array $data
 * @return array
 */
function returnResult($code, $message = '', $data = [])
{
    $output = array('code' => $code, 'message' => $message, 'data' => $data);
    return $output;
}
/**
 * 获取时间范围,
 * @param $type 1:近12月 2：近30天 3:近7天,4:具体某一天时间段
 * @return array
 */
function getTimeRange($type,$num,$include = false)
{
    $timeRange = [];
    switch ($type) {
        case 'MONTH':
            $timeRange = getNearlyXmonth($num,$include);//近X月
            break;
        case 'DAY':
            $timeRange = getNearlyXdays($num,$include);//近X天
            break;
        case 'TIME':
            $timeRange = getDaysTimeSlot($num);//昨天
            break;
        default:

    }
    return $timeRange;
}

/**
 * 获取近X天的日期
 * @param $num
 */
function getNearlyXdays($num, $including_today = false)
{
    $xAxis = [];
    $time = [];
    $num = $including_today ? $num : $num + 1;
    $start_i = $including_today ? 1 : 2;
    for ($i = $num; $i >= $start_i; $i--) {
        $date = date('Y-m-d', strtotime('-' . ($i - 1) . ' days'));
        array_push($xAxis, date('d', strtotime($date)) . '日');
        array_push($time,$date);
    }
    $start_time = mktime(0, 0, 0, date('m'), date('d') - $num+1, date('Y'));  //30日开始
    $end_time = mktime(23, 59, 59, date('m'), date('d')-($start_i-1), date('Y'));  //今日结束
    return ['xAxis' => $xAxis, 'time' => $time,'start_time'=>$start_time,'end_time'=>$end_time];
}

/**
 * 获取近X月的日期
 * @param $days
 */
function getNearlyXmonth($num, $including_month = false)
{
    $xAxis = [];
    $time = [];
    $num = $including_month?$num:$num+1;
    $start_i = $including_month ? 1 :2;
    for ($i = $num; $i >= $start_i; $i--) {
        $date = date('Y-m', strtotime('-' . ($i - 1) . ' month'));
        array_push($xAxis, date('m', strtotime($date)) . '月');
        array_push($time, date('Y-m', strtotime($date)));
    }
    $start_time = strtotime($time[0].'-1 00:00:00');
    $firstday = date('Y-m-01', strtotime($time[count($time)-1]));
    $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
    $end_time = strtotime($lastday.' 23:59:59');
    return ['xAxis' => $xAxis, 'time' => $time,'start_time'=>$start_time,'end_time'=>$end_time];
}

/**
 * 获取某一天时间段,若昨天传-1
 * @param int $days
 * @return array
 */
function getDaysTimeSlot($days = 0)
{
    $xAxis = [];
    $time = [];
    for ($i = 0; $i <= 24; $i += 2) {
        array_push($xAxis, $i . '时');
        $date = date('Y-m-d', strtotime($days . ' days'));
        array_push($time, $date . ' ' . $i . ':00:00');
    }
    $start_time = mktime(0, 0, 0, date('m'), date('d') + $days, date('Y'));  //30日开始
    $end_time = mktime(23, 59, 59, date('m'), date('d')+$days, date('Y'));  //今日结束
    return ['xAxis' => $xAxis, 'time' => $time,'start_time'=>$start_time,'end_time'=>$end_time];
}
//根据经纬度计算距离
function getDistance($lat1, $lng1, $lat2, $lng2)
{
    $earthRadius = 6367000; //approximate radius of earth in meters

    $lat1 = ($lat1 * pi()) / 180;
    $lng1 = ($lng1 * pi()) / 180;

    $lat2 = ($lat2 * pi()) / 180;
    $lng2 = ($lng2 * pi()) / 180;

    $calcLongitude = $lng2 - $lng1;
    $calcLatitude = $lat2 - $lat1;
    $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
    $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
    $calculatedDistance = $earthRadius * $stepTwo;

    return round($calculatedDistance);
}
/**
 * 获取随机字符串
 * @param int $randLength 长度
 * @return string
 */
function rand_str($randLength = 8)
{
    $chars = '0123456789';
    $len = strlen($chars);
    $randStr = '';
    for ($i = 0; $i < $randLength; $i++) {
        $randStr .= $chars[mt_rand(0, $len - 1)];
    }
    $tokenvalue = $randStr;
    return $tokenvalue;
}


