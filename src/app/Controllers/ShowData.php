<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018-08-24
 * Time: 下午 5:31
 */

namespace app\Controllers;

use Server\CoreBase\Controller;
use Server\SwooleMarco;


class ShowData extends Base
{

    protected $EquipmentlistsModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->EquipmentlistsModel = $this->loader->model('EquipmentlistsModel', $this);
    }


    /**
     *  封装发送消息
     * @author bg
     * @date 2018-08-24 下午 5:39
     */
    protected function sendMessage($uid, $data)
    {
        if (is_array($uid)) {
            $this->sendToUids($uid, $data);
        } else {
            $this->sendToUid($uid, $data);
        }
    }

    public function jsonToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }

    /**
     *  心跳包
     * @author bg
     * @date 2018-08-24 下午 5:39
     */
    public function A0()
    {
        $dataArr = $this->jsonToArray($this->client_data->data);
        $token = date('YmdHis') . 'uid' . rand(100, 999);
        $init = false;
        if (empty($this->uid) || $this->uid != $token) {
            $this->bindUid($token);
            $init = true;
        }
        $sendData = ['command' => 'A0', 'data' => []];
        $this->send($sendData);
        if ($init) {
            $data = $this->EquipmentlistsModel->get_A(1);
            foreach ($data as $key => $value) {
                $sendData = ['command' => $key, 'data' => $value];
                $this->sendMessage($this->uid, $sendData);
            }
        }
    }

    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/10/19 15:35
     * @title 发送推送消息
     */
    public function http_A10()
    {
        $parm = json_decode($this->http_input->getRawContent(), true);
        $data = $parm['data'];
        $flag = $parm['flag'];
        if ($flag == 1) {
            $A_data = $this->EquipmentlistsModel->get_A(1);
            foreach ($A_data as $key => $value) {
                $sendData = ['command' => $key, 'data' => $value];
                $this->sendToAll($sendData);
            }
            $this->jsonend(1000, 'A10', $A_data);
        }
        $sendData = ['command' => 'A10', 'data' => $data];
        $this->sendToAll($sendData);
    }

    public function http_get()
    {
        $parm = json_decode($this->http_input->getRawContent(), true);
        $data = $this->EquipmentlistsModel->get_A($parm['type']);
        $this->jsonend(1000, 'GET', $data);
    }


}