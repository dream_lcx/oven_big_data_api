<?php

/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-15
 * Time: 下午3:51
 */

namespace app\Controllers;

use app\Actors\TestActor;
use app\Models\TestModel;
use app\Models\UserModel;
use app\Models\WaterCard;
use app\Unit\Common;
use Firebase\JWT\JWT;
use Server\Asyn\Mysql\Miner;
use Server\Asyn\Mysql\MySqlCoroutine;
use Server\Asyn\TcpClient\SdTcpRpcPool;
use Server\Components\CatCache\CatCacheRpcProxy;
use Server\Components\CatCache\TimerCallBack;
use Server\Components\Consul\ConsulServices;
use Server\Components\Event\EventCoroutine;
use Server\Components\Event\EventDispatcher;
use Server\CoreBase\Actor;
use Server\CoreBase\Controller;
use Server\CoreBase\SelectCoroutine;
use Server\Memory\Cache;
use Server\Memory\Lock;
use Server\Tasks\TestTask;

class TestController extends Controller {

    /**
     * @var TestTask
     */
    public $testTask;

    /**
     * @var TestModel
     */
    public $testModel;
    public $user_model;
    public $parm;
    protected $equiModel;
    protected $equiStatusModel;
    protected $uids;

    /**
     * @var SdTcpRpcPool
     */
    public $sdrpc;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
    }


    public function onConnect(){
        var_dump('设备已连接: '.$this->fd.'-'.$this->uid);
    }

    public function onMessage(){
        var_dump('接受的数据'.$this->client_data);
        $data = json_decode($this->client_data,true);
        var_dump($data);
        if(isset($data['ping'])){
            $this->bindUid($data['ping']);
        }
        if(isset($data['msg'])){
            $this->sendToUids([1,2],'用户'.$this->uid.' : '.$data['msg']);
        }
    }


    public function onClose(){
      //  $this->sendToAll('用户'.$this->uid.' : 已关闭连接');
        var_dump('用户'.$this->uid.' : 已关闭连接');
      //  $this->kickUid($this->uid);
    }

}
