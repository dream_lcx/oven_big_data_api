<?php

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/12 0012
 * Time: 下午 3:13
 */

namespace app\Unit;


use app\Models\WaterCardModel;
use app\Unit\Network\PushCommand;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class Common
{
    static function getTime()
    {
        return time();
    }

    /*
     * 生成编号
     */
    static function order_sn()
    {

        return date('Y') . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
    }

    /*
     * microsecond 微秒     millisecond 毫秒
     *返回时间戳的毫秒数部分
     */
    function get_millisecond()
    {
        list($usec, $sec) = explode(" ", microtime());
        $msec = round($usec * 1000);
        return $msec;

    }

    /**
     * 传递参数 生成编号
     * @param string $head
     * @return string
     */
    static function pay_sn($head)
    {
        return strtoupper($head . dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
    }

    /**
     * 验证手机号是否正确
     *
     * @param INT $mobile
     * @return bool
     * @author
     *
     */
    static function isMobile($mobile)
    {
        return preg_match('#^1[3-9][\d]{9}$#', $mobile) ? true : false;
    }

    /**
     * 密码加密
     * @param String $password
     * @param String $salt
     * @return string
     */
    static function toPassword($password, $salt = "USER123456")
    {
        $password_code = md5(md5($password . '_' . $salt) . $salt);
        return $password_code;
    }

    /**
     * 手机归属地查询
     * @param string $phone
     * @return array|string
     */
    static function attribution($phone)
    {
        $url = "http://apis.juhe.cn/mobile/get?phone=" . $phone . "&key=d87d7e35d6ffc35dfb0fc4ac7ae12d9a";
        $result = file_get_contents($url);
        $result = json_decode($result, true);
        return $result['result']['province'];
    }

    /**
     * 验证邮箱
     * @param String $email
     * @return bool
     */
    static function isEmail($email)
    {
        $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
        if (preg_match($pattern, $email)) {
            return true;
        }
        return false;
    }

    // 判断是否是微信浏览器
    static function isWeixin()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断身份证号码格式
     * @param string $SFZ 身份证号码
     * @return bool
     */
    static function FunIsSFZ($SFZ)
    {
        $Run = @preg_match('/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/', $SFZ);
        return $Run;
    }

    /**
     * 数组数据加密并编译为字符串
     * @param array $Arr
     * @return array|string
     */
    static function EncodeArr($Arr)
    {
        $String = "";
        if (is_array($Arr)) {
            $String = base64_encode(serialize($Arr));
        } else {
            $String = "ST:" . base64_encode($Arr);
        }
        return $String;
    }

    /**
     * 数据数据解密并还原为数组
     * @param string $String
     * @return array|string
     */
    static function DecodeArr($String)
    {
        $Arr = array();
        if (!empty($String)) {

            if (preg_match('/^ST\:/', $String)) {
                $Arr = base64_decode(substr($String, 2));
            } else {

                $Row = @unserialize(base64_decode($String));
                if (!empty($Row) && is_array($Row)) {
                    $Arr = $Row;
                }
            }
        }
        return $Arr;
    }

    //钱的格式，两位小数
    static function moneyFormat($money)
    {
        return sprintf('%.2f', $money);
    }

    /**
     * 排序（从大到小）
     * @param $array
     * @return array
     * @throws \Exception
     * @author ligang
     */
    static function sortArray($array)
    {
        if (!is_array($array)) {
            throw new \Exception("排序类型错误");
        }
        $len = count($array);
        for ($i = 1; $i < $len; $i++) {
            for ($j = $len - 1; $j >= $i; $j--) {
                if ($array[$j] >= $array[$j - 1]) {
                    $x = $array[$j];
                    $array[$j] = $array[$j - 1];
                    $array[$j - 1] = $x;
                }
            }
        }
        return $array;
    }

    /**
     * 排序（从大到小）
     * @param array $array 二维数组
     * @return array
     * @throws \Exception
     * @author ligang
     */
    static function sortTwoArray($array, $field)
    {
        if (!is_array($array)) {
            throw new \Exception("排序类型错误");
        }
        $len = count($array);
        for ($i = 1; $i < $len; $i++) {
            for ($j = $len - 1; $j >= $i; $j--) {
                if ($array[$j][$field] >= $array[$j - 1][$field]) {
                    $x = $array[$j];
                    $array[$j] = $array[$j - 1];
                    $array[$j - 1] = $x;
                }
            }
        }
        return $array;
    }

    /**
     * 转换时间
     * @param array $array 需要转换的数组
     * @param string|array $field 被转换的字段(可以是数组，一维数组，如：['timeField'])
     * @param int $type 转换类型，1：转日期，2：转时间戳
     * @param string $format 转换的格式
     * @return array
     * @date 2018/3/16 17:22
     * @author ligang
     */
    static function formatTime(array $array, $field, int $type = 1, string $format = 'Y-m-d H:i:s')
    {
        if (empty($array)) {
            return $array;
        }
        if (count($array) == count($array, 1)) {
            if ($type == 1) {
                if (is_array($field)) {
                    foreach ($field as $k => $v) {
                        if (!empty($array[$v])) {
                            $array[$v] = date($format, $array[$v]);
                        } else {
                            $array[$v] = "——";
                        }
                    }
                } else {
                    if (!empty($array[$field])) {
                        $array[$field] = date($format, $array[$field]);
                    } else {
                        $array[$field] = "——";
                    }
                }
            } else {
                if (is_array($field)) {
                    foreach ($field as $k => $v) {
                        if (!empty($array[$v])) {
                            $array[$v] = strtotime($array[$v]);
                        } else {
                            $array[$v] = "——";
                        }
                    }
                } else {
                    if (!empty($array[$field])) {
                        $array[$field] = strtotime($array[$field]);
                    } else {
                        $array[$field] = "——";
                    }
                }
            }
            return $array;
        } else {
            if ($type == 1) {
                foreach ($array as $key => $value) {
                    if (is_array($field)) {
                        foreach ($field as $k => $v) {
                            if (!empty($value[$v])) {
                                $array[$key][$v] = date($format, $value[$v]);
                            } else {
                                $array[$key][$v] = "——";
                            }
                        }
                    } else {
                        if (!empty($value[$field])) {
                            $array[$key][$field] = date($format, $value[$field]);
                        } else {
                            $array[$key][$field] = "——";
                        }
                    }
                }
            } else {
                foreach ($array as $key => $value) {
                    if (is_array($field)) {
                        foreach ($field as $k => $v) {
                            if (!empty($value[$v])) {
                                $array[$key][$v] = strtotime($value[$v]);
                            } else {
                                $array[$key][$v] = "——";
                            }
                        }
                    } else {
                        if (!empty($value[$field])) {
                            $array[$key][$field] = strtotime($value[$field]);
                        } else {
                            $array[$key][$field] = "——";
                        }
                    }
                }
            }
            return $array;
        }
    }

    /**
     * 转换地址
     * @param array $array 需要转换的数组
     * @param string $field 省级字段名
     * @param array $mergeField 需要合并的字段数组（一维）
     * @return array
     * @date 2018/3/16 17:22
     * @author ligang
     */
    static function formatAddress(array $array, string $field, array $mergeField)
    {
        //直辖市
        $MDUYCG = ['北京市', '上海市', '天津市', '重庆市'];
        if (count($array) == count($array, 1)) {
            $array['merge_address'] = '';
            if (in_array($array[$field], $MDUYCG)) {
                foreach ($mergeField as $key => $value) {
                    if ($value != $field) {
                        $array['merge_address'] .= $array[$value];
                    }
                }
            } else {
                foreach ($mergeField as $key => $value) {
                    $array['merge_address'] .= $array[$value];
                }
            }
            return $array;
        } else {
            foreach ($array as $key => $value) {
                $array[$key]['merge_address'] = '';
                if (in_array($value[$field], $MDUYCG)) {
                    foreach ($mergeField as $k => $v) {
                        if ($v != $field) {
                            $array[$key]['merge_address'] .= $array[$key][$v];
                        }
                    }
                } else {
                    foreach ($mergeField as $k => $v) {
                        $array[$key]['merge_address'] .= $array[$key][$v];
                    }
                }
            }
            return $array;
        }
    }

    /**
     * 补位
     * @param string $string 目标字符串
     * @param string $cover 补位字符串
     * @param int $length 补位后总长度
     * @param bool $position 补位位置（前|后）
     * @return string
     * @date 2018/3/30 14:14
     * @author ligang
     */
    static function cover(string $string, string $cover = '0', int $length = 4, bool $position = true)
    {
        $strLen = strlen($string);
        if ($length - $strLen < 0) {
            return $string;
        }
        if ($position) {
            $tmp = '';
            for ($i = 1; $i <= $length - $strLen; $i++) {
                $tmp .= $cover;
            }
            return $tmp . $string;
        } else {
            $tmp = '';
            for ($i = 1; $i <= $length - $strLen; $i++) {
                $tmp .= $cover;
            }
            return $string . $tmp;
        }
    }

    /**
     * 判断是否是json
     * @param string $string
     * @return bool
     * @author ligang
     * @date 2018/4/12 16:21
     */
    static function checkJson($string)
    {
        return is_null(json_decode($string));
    }

    /**
     * 获取每月的开始时间和结束时间
     * 请求参数     请求类型      是否必须      说明
     * date         string       是            时间(格式：Y-m-d)
     * @param $date
     * @return array
     * @date 2018/4/19 10:45
     * @author ligang
     */
    static function getTheMonth($date)
    {
        $firstday = date('Y-m-01 00:00:00', strtotime($date)); //月初
        $lastday = date('Y-m-d 23:59:59', strtotime("$firstday +1 month -1 day"));//月末
        return array($firstday, $lastday);
    }

    /**
     * 获取每天的开始时间和结束时间
     * 请求参数     请求类型      是否必须      说明
     * date         string          是           日期（格式：Y-m-d）
     * @param $date
     * @return array
     * @date 2018/4/19 11:22
     * @author ligang
     */
    static function getTheDay($date, $type = 1)
    {
        if ($type == 1) {
            return [$date . ' 00:00:00', $date . ' 23:59:59'];
        }
        return [strtotime($date . ' 00:00:00'), strtotime($date . ' 23:59:59')];
    }


    /**
     * 字符串替换
     * @param string $string 被替换的字符
     * @param string $re_str 替换的字符
     * @param bool $rule 规则（true：等长度替换，false：不等长度替换）
     * @param int $length 长度（首位长度）
     * @param int $end_len 末尾长度
     * @return string
     * @date 2018/4/23 11:02
     * @author ligang
     */
    static function stringReplace($string, string $re_str = '*', bool $rule = true, int $length = 4, int $end_len = 0)
    {
        if (empty($string)) {
            return false;
        }
        $end_len = $end_len == 0 ? $length : $end_len;
        $str_len = strlen($string);
        if ($rule) {
            $tmp = '';
            for ($i = 1; $i <= $str_len - $length * 2; $i++) {
                $tmp .= $re_str;
            }
            return substr($string, 0, $length) . $tmp . substr($string, -$end_len);
        } else {
            $tmp = '';
            for ($i = 1; $i <= $length; $i++) {
                $tmp .= $re_str;
            }
            return substr($string, 0, $length) . $tmp . substr($string, -$end_len);
        }
    }

    /**
     * 替换null
     * @param array $array 需要检查的数组(仅支持一维、二维数组)
     * @param string $str null替换的字符串
     * @return array
     * @date 2018/6/15 15:30
     * @author ligang
     */
    static function replaceNull(array $array, string $str = '')
    {
        if (empty($array)) {
            return $array;
        }

        if (count($array) == count($array, 1)) {
            foreach ($array as $key => $value) {
                if (is_null($value)) {
                    $array[$key] = $str;
                }
            }
        } else {
            foreach ($array as $key => $value) {
                foreach ($value as $k => $v) {
                    if (is_null($v)) {
                        $array[$key][$k] = $str;
                    }
                }
            }
        }
        return $array;
    }

    /**
     * 根据地址获取距离或经纬度
     * @param string $address 地址
     * @param float $lng 经度
     * @param float $lat 纬度
     * @param bool $is_all 返回经纬度和距离
     * @return array|float|int
     * @date 2018/10/23 11:44
     * @author
     */
    static function getlocationByaddress(string $address, float $lng, float $lat, bool $is_all = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://apis.map.qq.com/ws/geocoder/v1/?address=" . $address . "&key=2CZBZ-RKRK3-HGJ3G-YH2G2-GIL3T-LHBFD");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $map = json_decode($data);
//        var_dump($map);
        $location = $map->result->location;
        if ($is_all) {
            return $result = [
                'lng' => $location->lng,
                'lat' => $location->lat,
                'distance' => Common::getdistance($lng, $lat, $location->lng, $location->lat)
            ];
        }
        return Common::getdistance($lng, $lat, $location->lng, $location->lat);
    }

    static function getdistance($lng1, $lat1, $lng2, $lat2)
    {
        // 将角度转为狐度
        $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
        $s = round($s, 2);
        return $s;
    }

    /**
     * Notes: redis的时间搜索
     * @param array $array
     * @param array $param 条件参数
     * @param string $field 时间字段
     * @param string $start_time 开始时间参数
     * @param string $end_time 结束时间参数
     * @return array
     */
    static function redisArraySearch(array $array, array $param, string $field, string $start_time = 'start_time', string $end_time = 'end_time')
    {
        $start_time = $param[$start_time] ?? '';
        $end_time = $param[$end_time] ?? '';
        $newList = [];
        foreach ($array as $key => $val) {
            $flag = count($param);
            //时间条件
            if (!empty($start_time)) {
                if (!empty($end_time)) {
                    if ($start_time <= date('Y-m-d', $val[$field]) && date('Y-m-d', $val[$field]) <= $end_time) {
                        $flag -= 2;
                    } else {
                        $flag += 2;
                    }
                } else {
                    if ($start_time < date('Y-m-d', $val[$field])) {
                        $flag--;
                    } else {
                        $flag++;
                    }
                }
            } else {
                if (!empty($end_time)) {
                    if (date('Y-m-d', $val[$field]) < $end_time) {
                        $flag--;
                    } else {
                        $flag++;
                    }
                }
            }
            foreach ($param as $k => $v) {
                if (array_key_exists($k, $val)) {
                    if ($k == 'equipment_number') {
                        if (stristr($val[$k], $v)) {
                            $flag--;
                        }
                    } else if ($val[$k] == $v) {
                        $flag--;
                    } else {
                        $flag++;
                    }
                }
            }
            if ($flag == 0) {
                $newList[] = $val;
            }
        }
        return $newList;
    }


    /**
     * Notes:分页
     * @param array $array
     * @param $page
     * @param $pageSize
     * @return array
     */
    static function page(array $array, $page, $pageSize)
    {
        $list = [];
        $nowPage = ($page - 1) * $pageSize;
        $allPage = $nowPage + $pageSize;
        if ($page == 1) {
            $nowPage = 0;
            $allPage = $pageSize;
        }
        for ($i = $nowPage; $i < $allPage; $i++) {
            if (isset($array[$i])) {
                array_push($list, $array[$i]);
            }
        }
        return $list;
    }


    static function sendWxPush($openid, $templateKey, array $data, $application)
    {
        $client = new \swoole_client(SWOOLE_TCP, SWOOLE_SYNC);
        $client->set(array(
            'open_length_check' => true, //打开EOF检测
            'package_length_type' => 'N', //设置EOF
            'package_length_offset' => 0,
            'package_body_offset' => 4,
        ));
        $client->connect('127.0.0.1', 9501, 3);
        if (!empty($openid) && !empty($templateKey) && strlen($application) && !empty($data)) {
            $command = new PushCommand(1, $openid, $templateKey, $data, $application);
            $message = $command->encode();
            $client->send($message);
        }
    }


    /**
     * Notes:邮箱发送
     * @param $mail
     * @param array $addressArr
     * @param string $email
     * @param string $auth
     * @param string $title
     * @param string $contect
     * @param string $name
     * @return mixed
     */
    static function sendMail($mail, $addressArr = [], $email = '', $auth = '', $title = '', $contect = '', $name = '')
    {
        // 是否启用smtp的debug进行调试 开发环境建议开启 生产环境注释掉即可 默认关闭debug调试模式
        $mail->SMTPDebug = 1;
        // 使用smtp鉴权方式发送邮件
        $mail->IsSMTP();
        // smtp需要鉴权 这个必须是true
        $mail->SMTPAuth = true;
        // 链接qq域名邮箱的服务器地址
        $mail->Host = 'smtp.163.com';
        // 设置使用ssl加密方式登录鉴权
        $mail->SMTPSecure = 'ssl';
        // 设置ssl连接smtp服务器的远程服务器端口号
        $mail->Port = 465;
        // 设置发送的邮件的编码
        $mail->CharSet = 'UTF-8';
        // 设置发件人昵称 显示在收件人邮件的发件人邮箱地址前的发件人姓名
        $mail->FromName = $name;
        // smtp登录的账号 QQ邮箱即可
        $mail->Username = $email;
        // smtp登录的密码 使用生成的授权码
        $mail->Password = $auth;
        // 设置发件人邮箱地址 同登录账号
        $mail->From = $email;
        // 邮件正文是否为html编码 注意此处是一个方法
        // 设置收件人邮箱地址
        // 添加多个收件人 则多次调用方法即可
        foreach ($addressArr as $k => $v) {
            $mail->addAddress($v);
        }
        // 添加该邮件的主题
        $mail->Subject = $title;
        // 添加邮件正文
        $mail->Body = $contect;
        // 为该邮件添加附件
//        $mail->addAttachment('./example.pdf');
        // 发送邮件 返回状态
        $status = $mail->send();
        return $status;
    }

    /**
     * 拼接七牛云图片路径
     * @param $img
     */
    static function picUrl($img)
    {
        $config = get_instance()->config;
        $qiniu = $config->get('qin_niu');
        if (is_array($img)) {
            foreach ($img as &$v) {
                $v = $qiniu . $v;
            }
        } else {
            $img = $qiniu . $img;
        }
        return $img;
    }

    /**
     * 生成excel
     * @param array $data
     * @param array $tableTitle
     * @param array $config
     * @param array $tableValue
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    static function export(array $data, array $tableTitle, array $config, array $tableValue)
    {
        $spreadsheet = new Spreadsheet();

        //表中标题
        $title = $config['title'] ?? '标题';

        //设置sheet名
        $spreadsheet->getActiveSheet()->setTitle($config['sheet_title'] ?? 'sheet1');

        //设置表头
        $letter = 'A';
        foreach ($tableTitle as $ttk => $ttv) {
            $spreadsheet->getActiveSheet()->setCellValue($letter . '2', $ttv);
            //设置列宽
            $spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(30);
            if (count($tableTitle) - 1 != $ttk) {
                $letter++;
            }
        }
        $titleStyleArray = [
            'font' => [
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        //合并单元格，并设置标题和样式
        $spreadsheet->getActiveSheet()->mergeCells('A1:' . $letter . '1');
        $spreadsheet->getActiveSheet()->setCellValue('A1', $title);
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($titleStyleArray);

        //设置数据部分样式
        $dataStyleArray = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        $spreadsheet->getActiveSheet()->getStyle('A2:' . $letter . (count($data) + 3))->applyFromArray($dataStyleArray);

        //设置行高(参数：pRow是表的行数，从一开始)
        $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(50);

        //设置单元格值
        $k = 3;
        foreach ($data as $key => $value) {
            foreach ($tableValue as $i => $v) {
                $spreadsheet->getActiveSheet()->setCellValue($i . $k, $value[$v]);
            }
            $k++;
        }

        $filename = md5(date('YmdHis', time()) . uniqid()) . '.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $path = WWW_DIR . '/partner/excel/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $writer->save($path . $filename);
        return '/excel/' . $filename;
    }
}


