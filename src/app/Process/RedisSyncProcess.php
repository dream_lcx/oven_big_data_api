<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-03-15
 * Time: 下午 3:51
 */

namespace app\Process;

use Server\Components\Process\Process;
use Server\Asyn\Redis\RedisAsynPool;
use Server\Asyn\Mysql\MysqlAsynPool;

class RedisSyncProcess extends Process
{
    protected $mysql;
    protected $redis;
    protected $loader;

    protected $deviceListKey = 'deviceList';
    protected $devTableName = 'equipment';
    protected $times = 300;
   // protected $times = 60;

    public function start($process)
    {
//        $this->redisPool = new RedisAsynPool($this->config, $this->config->get('redis.active'));
//        $this->mysqlPool = new MysqlAsynPool($this->config, $this->config->get('mysql.active'));
//
//        get_instance()->addAsynPool("redisPool", $this->redisPool);
//        get_instance()->addAsynPool("mysqlPool", $this->mysqlPool);
//        $this->mysql = get_instance()->getAsynPool('mysqlPool')->installDbBuilder();
//        $this->redis = get_instance()->getAsynPool('redisPool')->getCoroutine();
        //下面是定时器
//        swoole_timer_tick(1000 * $this->times, function () {
//            $this->__do();
//        });

    }

    public function __do()
    {
        try {
            //从redis获取设备列表
            $redisDevList = $this->redis->hVals($this->deviceListKey);
            $onWhere = [];
            $offWhere = [];
            if (empty($redisDevList)) {
                return true;
            }
            foreach ($redisDevList as $key => $val) {
                $devInfo = json_decode($val, true);
                if (empty($devInfo['equipment_number'] ?? '')) {
                    continue;
                }
                $isOnline = get_instance()->coroutineUidIsOnline($devInfo['equipment_number']);
                var_dump(date('Y-m-d H:i:s'));
                var_dump($devInfo['equipment_number'].'-'.$isOnline);
                if ($isOnline) {//在线
                    array_push($onWhere, $devInfo['equipment_number']);
                    if (empty($devInfo['status']??'') || $devInfo['status'] != 1) {
                        $devInfo['status'] = 1;
                        //写连接记录
//                        $on_line_time['equipment_number'] = $devInfo['equipment_number'];
//                        $on_line_time['is_online'] = 1;
//                        $on_line_time['create_time'] = time();
//                        $this->mysql->insert('equipment_off_line_time')->set($on_line_time)->query()->insert_id();
                        $this->redis->hSet('deviceList', $devInfo['equipment_number'], json_encode($devInfo));
                    }

                } else{//离线
                    array_push($offWhere, $devInfo['equipment_number']);
                    if (!empty($devInfo['status']??'') && $devInfo['status'] != 2) {
                        $devInfo['status'] = 2;
                        //写连接记录
//                        $on_line_time['equipment_number'] = $devInfo['equipment_number'];
//                        $on_line_time['is_online'] = 0;
//                        $on_line_time['create_time'] = time();
//                        $this->mysql->insert('equipment_off_line_time')->set($on_line_time)->query()->insert_id();
                        $this->redis->hSet('deviceList', $devInfo['equipment_number'], json_encode($devInfo));
                        $this->mysql->update($this->devTableName)->where('equipment_number', $devInfo['equipment_number'])->set('off_line_time',time())->query();
                    }
                }
            }
            //修改在线的设备
            if ($onWhere) {
                $this->mysql->update($this->devTableName)->whereIn('equipment_number', $onWhere)->set('status', 1)->query();

            }
            //修改离线的设备
            if ($offWhere) {
                $this->mysql->update($this->devTableName)->whereIn('equipment_number', $offWhere)->set('status', 2)->query();
            }
        } catch (\Throwable $e) {
            var_dump("RedisSyncProcess异常:" . $e->getMessage() . $e->getLine());
        }
    }

    public function getData()
    {
        return '123';
    }

    protected function onShutDown()
    {
        echo '自定义进程RedisSyncProcess退出';
        // TODO: Implement onShutDown() method.
    }
}