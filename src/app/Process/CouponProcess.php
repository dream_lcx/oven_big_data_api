<?php

/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 17-8-15
 * Time: 上午10:52
 */

namespace app\Process;

use Server\Components\Process\Process;
use Server\Asyn\Mysql\MysqlAsynPool;

/**
 * 设置优惠券过期
 */
class CouponProcess extends Process
{

    protected $redisPool;
    protected $mysqlPool;
    protected $ick_id = '';
    protected $code_table_name = 'coupon_code';
    protected $db;

    public function start($process)
    {

//        $this->mysqlPool = new MysqlAsynPool($this->config, $this->config->get('mysql.active'));
//        $this->db = $this->mysqlPool->installDbBuilder();
        //$this->setCouponOverdue();
    }

    //优惠券过期操作
    public function setCouponOverdue()
    {

        $where = ['ccode_use_state' => 0, 'ccode_exchange_state' => 2, 'water_coupon.coupon_e_term' => ['<=', time()]];
        $join = [
            ['coupon', 'water_coupon.coupon_id = water_coupon_code.coupon_id', 'inner']
        ];
        $this->tick_id = get_instance()->tick(300000, function () use ($where, $join) {
            try {
                $cou = $this->db->select("*")->from($this->code_table_name)->TPWhere($where)->TPJoin($join)->query()->result_array();

                if (empty($cou)) {
                    return true;
                }

                foreach ($cou as $k => $v) {
                    $result = $this->db
                        ->update($this->code_table_name)
                        ->set(['ccode_use_state' => 2])
                        ->TPWhere(['ccode_id' => $v['ccode_id']])
                        ->query()
                        ->affected_rows();
                    //var_dump($result . '-------------' . $v['ccode_id']);
                }
                var_dump($this->db);
            } catch (\Throwable $e) {
                $this->mysqlPool = new MysqlAsynPool($this->config, $this->config->get('mysql.active'));
                $this->db = $this->mysqlPool->installDbBuilder();
                var_dump("执行优惠券过期操作异常-" . $e->getMessage() . $e->getCode());
            }
            return true;
        });
    }

    protected function onShutDown()
    {
       // get_instance()->clearTimer($this->tick_id);
        // TODO: Implement onShutDown() method.
    }

}
