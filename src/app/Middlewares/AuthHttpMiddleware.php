<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/12 0012
 * Time:      11:04
 */
namespace app\Middlewares;


use Server\Components\Middleware\HttpMiddleware;
use Firebase\JWT\JWT;

/*
 * 自定义中间件处理是否授权
 */
class AuthHttpMiddleware extends HttpMiddleware {
    public function __construct()
    {
        parent::__construct();
    }
    private $module;
    private $controller;
    private $action;
    public function before_handle()
    {
        $route = explode('/', $this->request->server['path_info']);
        $this->request->user_id = 0;
        $this->request->login_type = 0;
        $this->request->role = 0; //角色  1维修人员  2销售 3合伙人 4用户
        $this->request->user_openid = $this->request->header['openid']??'';
        $this->request->company_no = $this->request->header['company-no']??20001;
        if(!$this->filter()){
       // if(0){
            $token = $this->request->header['token']??'';
            if(empty($token)){                
                $this->jsonend(-1004,'token为空');
                $this->interrupt();
            }
            $key = $this->config['token_key'];

            try {
                $user_info = JWT::decode($token, $key, array('HS256'));
            } catch (\UnexpectedValueException $ex) {
                $this->jsonend(-1004,'用户信息错误');
                $this->interrupt();
            }
            if(empty($user_info)){
                $this->jsonend(-1004,'用户信息错误');
            }
            if($user_info->expire<time()){
                $this->jsonend(-1004,'用户信息错误');
            }
            //保存用户信息            
            $this->request->user_id = $user_info->user_id;
            $this->request->login_type = $user_info->login_type ?? 0;
            $this->request->role = $user_info->role ?? 0;
            if(!empty($user_info->company_no)){
                $this->request->company_no = $user_info->company_no;
            }
            if (isset($user_info->partner_id)){
                $this->request->partner_id = $user_info->partner_id;
            }

        }
    }

    public function after_handle($path)
    {
        //$this->jsonend(-1000,'请求错误~');
        // TODO: Implement after_handle() method.
    }
    //验证需要登录 的接口
    private function filter()
    {
        $route = explode('/', $this->request->server['path_info']);
        $count = count($route);
        if ($count == 4) {
            $this->action = $route[$count - 1] ?? null;
            $this->controller = $route[$count - 2] ?? null;
            $this->module = $route[$count - 3] ?? null;
        }else{
            return true;
        }

        //不需要验证的板块
        if ($this->module == 'Common' || $this->module == 'Water' ){
            return true;
        }

        $interceptor = $this->config->get('custom.interceptor');
        if (key_exists($this->module, $interceptor) && key_exists($this->controller, $interceptor[$this->module]) && in_array($this->action, $interceptor[$this->module][$this->controller])) {
            return true;
        } else {
            return false;
        }
    }

    private function jsonend($code = '',$message='',$data='')
    {
        $output = array('code' =>$code,'message' =>$message,'data' =>$data);
        if (is_array($output)||is_object($output)) {
            $this->response->header('Content-Type','textml; charset=UTF-8');
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->header("Access-Control-Allow-Headers"," Origin, X-Requested-With, Content-Type, Accept,token,openid,Company-No");
            $this->response->header("Access-Control-Allow-Methods"," POST,OPTIONS");
            $this->response->header('Content-Type', 'application/json;charset=UTF-8');          
            $output = json_encode($output,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->interrupt();
    }
}
