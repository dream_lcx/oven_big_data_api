<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-15
 * Time: 下午3:11
 */

namespace Server\Route;


use Noodlehaus\ErrorException;
use Server\CoreBase\SwooleException;

class WaterRoute implements IRoute
{
    private $client_data;
    private $controller_name;
    private $method_name;
    private $command;
    private $device_sn;
    private $command_route;

    public function __construct()
    {
        $this->client_data = new \stdClass();
        $this->command_route = get_instance()->config['command']['route'];
    }

    /**
     * 设置反序列化后的数据 Object
     * @param $data
     * @return \stdClass
     * @throws SwooleException
     */
    public function handleClientData($data)
    {
        $this->client_data = $data;
        if (isset($data['command']) && !empty($data['command']) && array_key_exists($data['command'],$this->command_route)) {
            $this->command = $data['command'];
            $this->device_sn = $data['device_sn'];
            $this->controller_name = $this->command_route[$this->command]['c'];
            $this->method_name = $this->command_route[$this->command]['a'];
            return $this->client_data;
        } else {
           // throw new SwooleException($data['command'].'指令错误-1');
        }


    }

    /**
     * 处理http request
     * @param $request
     */
    public function handleClientRequest($request)
    {
        $this->client_data->path = $request->server['path_info'];
        $route = explode('/', $request->server['path_info']);
        $count = count($route);
        if ($count == 2) {
            $this->client_data->controller_name = $route[$count - 1] ?? null;
            $this->client_data->method_name = null;
            return;
        }
        $this->client_data->method_name = $route[$count - 1] ?? null;
        unset($route[$count - 1]);
        unset($route[0]);
        $this->client_data->controller_name = implode("\\", $route);
    }

    /**
     * 获取控制器名称
     * @return string
     */
    public function getControllerName()
    {
        return $this->controller_name;
    }

    /**
     * 获取方法名称
     * @return string
     */
    public function getMethodName()
    {
        return $this->method_name;
    }

    public function getPath()
    {
        return $this->client_data->path ?? "";
    }

    public function getParams()
    {
        return $this->client_data->params??null;
    }

    public function errorHandle(\Throwable $e, $fd)
    {
        var_dump($e->getMessage());
        //get_instance()->send($fd, "Error:" . $e->getMessage(), true);
        //get_instance()->close($fd);
    }

    public function errorHttpHandle(\Throwable $e, $request, $response)
    {
        //重定向到404
        $response->status(302);
        $location = 'http://' . $request->header['host'] . "/" . '404';
        $response->header('Location', $location);
        $response->end('');
    }
}